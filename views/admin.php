<?php
  session_start();
  if (!isset($_SESSION['usuario'])) {
    header('Location: ../index.php');
  } else {
    $claves = $_SESSION['usuario']['claves'];
    if (!in_array('SUD0', $claves) && !in_array('ADM01', $claves)) {
      header('Location: ../index.php');
    }
  }
?>
<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pastoral Juvenil de la Diócesis de Tampico - Fuego Nuevo 2019</title>
    <link rel="shortcut icon" href="../favicon.png">
    <link rel="stylesheet" href="../css/foundation.min.css">
    <link rel="stylesheet" href="../css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="../css/jquery-ui.structure.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- <link rel="stylesheet" href="../css/buttons.dataTables.min.css"> -->
    <link rel="stylesheet" href="../css/app.css">
  </head>
  <body class="hide">
    <?php require_once 'header.php'; ?>

    <main>
      <div class="medium callout">
        <div class="row column">
          <h2 class="text-center">Inscripciones</h2>
          <h5 class="title-primary">Listado de Inscripciones</h5>
          <!-- <h3 class="text-center" style="line-height:1">FUEGO NUEVO 2017</h3> -->
        </div>

        <div class="row column">
          <table id="dt-inscripciones" class="dataTable" data-init="false">
            <thead></thead>
          </table>
        </div>
      </div>
    </main>

    <div id="reveal-ver-mas" class="medium reveal" data-reveal>
      <div class="row column">
        <p class="lead">Información sobre la Inscripción de
          <strong id="nombre-comunidad"></strong> a la Disciplina de
          <strong id="nombre-disciplina"></strong>.
        </p>
      </div>

      <main>
        <div class="row">
          <div class="large-12 columns">
            <label>Comunidad:
              <strong id="comunidad">Comunidad 1</strong>
            </label>
          </div>
        </div>

        <div class="row">
          <div class="large-12 columns">
            <label>Disciplina:
              <strong id="disciplina"></strong>
            </label>
          </div>
        </div>

        <div class="row">
          <div class="large-12 columns">
            <label>Status:
              <strong id="status"></strong>
            </label>
          </div>
        </div>

        <div class="row">
          <div class="large-12 columns">
            <fieldset class="small fieldset">
              <legend>Datos del Coordinador</legend>

              <label>Nombre:
                <strong id="coordinador-nombre"></strong>
              </label>

              <label>Celular:
                <strong id="coordinador-celular"></strong>
              </label>

              <label>Email:
                <strong id="coordinador-email"></strong>
              </label>

              <label>Fecha de Nacimiento:
                <strong id="coordinador-fecha-nacimiento"></strong>
              </label>

              <label>Edad:
                <strong id="coordinador-edad"></strong>
              </label>
            </fieldset>
          </div>
        </div>

        <div class="row">
          <div class="large-12 columns">
            <fieldset id="no-hay-delegado" class="small fieldset hide">
              <legend>Datos del Delegado</legend>
              <i >No hay delegado</i>
            </fieldset>

            <fieldset id="datos-delegado" class="small fieldset">
              <legend>Datos del Delegado</legend>

              <label>Nombre:
                <strong id="delegado-nombre"></strong>
              </label>

              <label>Celular:
                <strong id="delegado-celular"></strong>
              </label>

              <label>Email:
                <strong id="delegado-email"></strong>
              </label>

              <label>Fecha de Nacimiento:
                <strong id="delegado-fecha-nacimiento"></strong>
              </label>

              <label>Edad:
                <strong id="delegado-edad"></strong>
              </label>
            </fieldset>
          </div>
        </div>

        <!-- Carta del Sacerdote -->
        <div class="row">
          <div class="large-12 columns">
            <label>Carta del Sacerdote:
              <a id="carta-sacerdote" target="_blank">Da clic aquí.</a>
            </label>
          </div>
        </div>

        <!-- Listado de Miembros -->
        <div class="row">
          <div class="large-12 columns">
            <label>Listado de Miembros:
              <a id="listado-miembros" target="_blank">Da clic aquí.</a>
            </label>
          </div>
        </div>

        <div class="row">
          <div class="large-12 columns">
            <fieldset class="small fieldset">
              <legend>Integrantes</legend>
              <div id="integrantes" class="row small-up-2 medium-up-2 large-up-2"></div>
            </fieldset>
          </div>
        </div>
      </main>

      <div class="row column">
        <button class="close-button" data-close aria-label="Close reveal" type="button">
          <span aria-hidden="true">&times;</span>
        </button>
        <button class="small button float-right" data-close>CERRAR</button>
      </div>
    </div>

    <?php require_once 'context-menu.php'; ?>
    <?php require_once 'footer.php'; ?>

    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/what-input.js"></script>
    <script src="../js/vendor/foundation.min.js"></script>
    <script src="../js/vendor/jquery.dataTables.min.js"></script>
    <script src="../js/vendor/jquery-ui.min.js"></script>
    <!-- <script src="../js/vendor/dataTables.buttons.min.js"></script>
    <script src="../js/vendor/dataTables.buttons.flash.min.js"></script> -->
    <script src="../js/app.js"></script>
    <script>
      $(document).ready(function() {
        var datos = [
          {
            id: 'status-submenu',
            accion: 'obtener-status-inscripciones',
            input: 'ul'
          }
        ];
        var datosCargados = 0;
        var columns = [
          {'title': '#', 'className': 'text-right', 'width': '1%'},
          {'title': '', 'className': 'td-id', 'visible': false},
          {'title': 'COMUNIDAD', 'width': '5%'},
          {'title': 'COORDINADOR', 'width': '5%'},
          {'title': 'DISCIPLINA', 'width': '5%'},
          {'title': 'RAMA', 'width': '5%'},
          {'title': 'NUMERO DE INTEGRANTES', 'className': 'text-center', 'width': '5%'},
          {'title': 'STATUS', 'className': 'td-status text-center', 'width': '5%'}
        ];
        var table = $('#dt-inscripciones').dataTable( {
          'language': {
            'url': '../json/datatables.spanish.lang.json'
          },
          'autoWidth': false,
          'scrollX': true,
          'pageLength': 25,
          'processing': true,
          'serverSide': true,
          'ajax': '../php/scripts/server_processing.php?o=inscripciones',
          'columns': columns,
          'order': [],
          "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "Todos"] ],
          'initComplete': function( settings, json ) {
            var api = this.api();
            table.attr('data-init', true);
            table.on('contextmenu', 'tr', function(evt) {
              var x = evt.pageX;
              var y = evt.pageY;
              var rowIdx = api.row(this).index();
              var id = api.row(rowIdx).data()[1];
              var status = $(this).find('.td-status').text();

              $('#context-menu').css({
                left: x + 'px',
                top: y + 'px'
              })
              .removeClass('hide')
              .data('id', id);

              // Quitar los disabled & deshabilitar el status que ya este elegido.
              $('#status-submenu').find('.ui-state-disabled')
                .removeClass('ui-state-disabled')
                .end()
                .find('li:contains("'+status+'")')
                .addClass('ui-state-disabled');
              return evt.preventDefault();
            });
          },
          'drawCallback': function( settings ) {
            var api = this.api();
            var start = settings._iDisplayStart;

            // Crear un consecutivo en la primera columna.
            api.column(0).nodes().each( function (cell, i) {
              start += 1;
              cell.innerHTML = start;
            });

            // Darle funcionalidad al dropdown proveniente del server-side.
            table.find('.dropdown').foundation();
          },
          'destroy': true,
          // 'dom': 'B<\"clear\">lfrtip',
          // 'buttons': [{
          //   extend: 'excel',
          //   text: 'Exportar Excel',
          //   exportOptions: {
          //     modifier: {
          //       search: 'none'
          //     },
          //     columns: ':visible'
          //   }
          // },
          // {
          //   extend: 'csv',
          //   text: 'Exportar CSV',
          //   exportOptions: {
          //     modifier: {
          //       search: 'none'
          //     },
          //     columns: ':visible'
          //   }
          // }]
        });
        var calcularEdad = function(rawDate) {
          if (rawDate === null) return '';
          var formattedDate = rawDate.split('/').reverse().join('-');
          var birthDate = new Date(formattedDate);
          var diffDate = new Date - birthDate;
          var ageDate = new Date(diffDate);
          ageDate.setHours(ageDate.getHours() - 24); // Hack.. no era exacto.
          var age = Math.abs(ageDate.getUTCFullYear() - 1970);
          if (isNaN(age)) {
            return false;
          }

          return age;
        };

        // Cargar datos.
        (function() {
          mostrarLoading();

          datos.forEach(function(dato, index) {
            var id = dato.id;
            var name = dato.name;
            var accion = dato.accion;
            var input = dato.input;
            var dataset = dato.dataset;
            var selector = id ? '#' + id : '[name="' + name + '"]';

            $.post('../php/api.php', {
              accion: accion
            }, function(response) {
              if (response.status === 'OK') {
                var data = response.data;

                // Revisar a que tipo de input se le daran los valores.
                switch (input) {
                  case 'ul':
                    for (var i = 0; i < data.length; i++) {
                      $(selector).append('<li>' +
                        '<div>' + data[i].nombre + '</div>' +
                      '</li>');

                      $(selector).find('li:last')
                        .data('status', data[i].id);
                    }
                  break;
                }

                // Si tiene un dataset, mandar a llamar su evento onchange.
                // if (dataset) {
                //   $(selector).trigger('change');
                // }

                datosCargados += 1;
              } else {
                mostrarMensaje(response.msg);
              }

              // Ya termino de cargar todos los datos.
              if (datosCargados === datos.length) {
                $('#context-menu').menu();
                ocultarLoading();
              }
            }, 'json').fail(function() {
              ocultarLoading();
              mostrarMensaje('Falló la conexión al servidor,' +
                ' por favor vuelve a intentarlo.');
            });
          });
        })();

        $('#status-submenu').on('click', 'li', function() {
          var status = $(this).data('status');

          mostrarLoading();
          $.post('../php/api.php', {
            accion: 'cambiar-status-inscripcion',
            inscripcion: $('#context-menu').data('id'),
            status: status
          }, function(response) {
            if (response.status === 'OK') {
              table.DataTable().draw();
            }

            ocultarLoading();
            mostrarMensaje(response.msg);
          }, 'json').fail(function() {
            ocultarLoading();
            mostrarMensaje('Falló la conexión al servidor,' +
              ' por favor vuelve a intentarlo.');
          });
        });

        $('#ver-mas').on('click', function() {
          mostrarLoading();
          $.post('../php/api.php', {
            accion: 'obtener-mas-informacion',
            inscripcion: $('#context-menu').data('id')
          }, function(response) {
            ocultarLoading();

            if (response.status === 'OK') {
              // $('#reveal-ver-mas').foundation('open');
              var data = response.data;
              var informacionBase = data.informacionBase;
              var integrantes = data.integrantes;

              $('#nombre-comunidad').html(informacionBase.comunidad);
              $('#nombre-disciplina').html(informacionBase.disciplina);

              $('#comunidad').html(informacionBase.comunidad);

              $('#coordinador-nombre').html(informacionBase.coordinadorNombre);
              $('#coordinador-celular').html(informacionBase.coordinadorCelular);
              $('#coordinador-email').html(informacionBase.coordinadorEmail);
              $('#coordinador-fecha-nacimiento').html(informacionBase.coordinadorFechaNacimiento);
              $('#coordinador-edad').html(function() {
                return calcularEdad(informacionBase.coordinadorFechaNacimiento) + ' años';
              });

              if (informacionBase.delegadoNombre === '') {
                $('#no-hay-delegado').removeClass('hide');
                $('#datos-delegado').addClass('hide');
              } else {
                $('#delegado-nombre').html(informacionBase.delegadoNombre);
                $('#delegado-celular').html(informacionBase.delegadoCelular);
                $('#delegado-email').html(informacionBase.delegadoEmail);
                $('#delegado-fecha-nacimiento').html(informacionBase.delegadoFechaNacimiento);
                $('#delegado-edad').html(function() {
                  return calcularEdad(informacionBase.delegadoFechaNacimiento) + ' años';
                });

                $('#no-hay-delegado').addClass('hide');
                $('#datos-delegado').removeClass('hide');
              }

              $('#carta-sacerdote').attr('href', informacionBase.carta);
              $('#listado-miembros').attr('href', informacionBase.lista);

              $('#disciplina').html(informacionBase.disciplina);
              $('#status').html(informacionBase.status);

              $('#integrantes').empty();
              integrantes.forEach(function(integrante, index) {
                if (integrante.nombre === '') return;
                var column = document.createElement('div');
                var card = document.createElement('div');
                var cardSection = document.createElement('div');
                var photo = document.createElement('img');

                column.classList.add('column');
                column.appendChild(card);
                card.appendChild(photo);
                card.appendChild(cardSection);
                card.classList.add('card');

                cardSection.classList.add('card-section');
                cardSection.innerHTML = '<div class="row column">' +
                  '<label>Nombre: ' +
                    '<strong>' + integrante.nombre + '</strong>' +
                  '</label>' +
                '</div>' +
                '<div class="row column">' +
                  '<label>Tipo de Integrante: ' +
                    '<strong>' + integrante.tipoIntegrante + '</strong>' +
                  '</label>' +
                '</div>' +
                '<div class="row column">' +
                  '<label>Fecha de Nacimiento: ' +
                    '<strong>' + integrante.fechaNacimiento + '</strong>' +
                  '</label>' +
                '</div>' +
                '<div class="row column">' +
                  '<label>Edad: ' +
                    '<strong>' + calcularEdad(integrante.fechaNacimiento) + '</strong>' +
                  '</label>' +
                '</div>' +
                '<div class="row column">' +
                  '<label>Credencial:' +
                    '<a href="' + integrante.credencial + '" target="_blank">Da clic aquí...</a>' +
                  '</label>' +
                '</div>';

                photo.src = integrante.fotografia;
                $('#integrantes').append(column);
              })

              $('#reveal-ver-mas').foundation('open');
              console.log(response.data);
            } else {
              mostrarMensaje(response.msg);
            }

          }, 'json').fail(function() {
            ocultarLoading();
            mostrarMensaje('Falló la conexión al servidor,' +
              ' por favor vuelve a intentarlo.');
          });
        });

        $(document).on('click', function() {
          $('#context-menu')
            .addClass('hide')
            .data('id', false);
        });

        $('body').removeClass('hide');
      });
    </script>
  </body>
</html>