<header>
  <div class="title-bar" data-responsive-toggle="menu" data-hide-for="medium">
    <button class="menu-icon" type="button" data-toggle="menu"></button>
    <div class="title-bar-title">Fuego Nuevo 2019</div>
  </div>

  <div class="top-bar" id="menu">
    <div class="top-bar-left">
      <ul class="dropdown menu" data-dropdown-menu>
        <li class="menu-text show-for-medium">Fuego Nuevo 2019</li>
        <?php
          if (in_array('SUD0', $claves) || in_array('FN01', $claves)) {
            echo '<li><a href="index.php">Formulario de Inscripción</a></li>';
          }

          if (in_array('SUD0', $claves) || in_array('ADM1N', $claves)) {
            $adminPanel = '<li>' .
                '<a href="./">Panel de Adminstración</a>' .
                '<ul class="menu vertical" style="margin-left: 1rem;">';

            if (in_array('SUD0', $claves) || in_array('ADM01', $claves)) {
              $adminPanel .= '<li><a href="admin.php">Inscripciones</a></li>';
            }

            if (in_array('SUD0', $claves) || in_array('ADM02', $claves)) {
              $adminPanel .= '<li><a href="communities.php">Comunidades</a></li>';
            }

            if (in_array('SUD0', $claves) || in_array('ADM03', $claves)) {
              $adminPanel .= '<li><a href="users.php">Usuarios</a></li>';
            }

            if (in_array('SUD0', $claves) || in_array('ADM04', $claves)) {
              $adminPanel .= '<li><a href="export.php">Exportar</a></li>';
            }

            $adminPanel .= '</ul>' .
              '</li>';

            echo $adminPanel;
          }
        ?>
      </ul>
    </div>
    <div class="top-bar-right">
      <ul class="menu">
        <li style="padding-right: 1rem;">¡Bienvenido
          <strong><?php echo $_SESSION['usuario']['username']; ?></strong>!
        </li>
        <li>
          <button id="salir" type="button" class="button">Cerrar Sesión</button>
        </li>
      </ul>
    </div>
  </div>
</header>