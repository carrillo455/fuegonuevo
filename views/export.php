<?php
  session_start();
  if (!isset($_SESSION['usuario'])) {
    header('Location: ../index.php');
  } else {
    $claves = $_SESSION['usuario']['claves'];
    if (!in_array('SUD0', $claves) && !in_array('ADM04', $claves)) {
      header('Location: ../index.php');
    }
  }
?>
<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pastoral Juvenil de la Diócesis de Tampico - Fuego Nuevo 2019</title>
    <link rel="shortcut icon" href="../favicon.png">
    <link rel="stylesheet" href="../css/foundation.min.css">
    <link rel="stylesheet" href="../css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  </head>
  <body class="hide">
    <?php require_once 'header.php'; ?>

    <main>
      <div class="medium callout">
        <div class="row column">
          <h1 class="text-center">Exportar</h1>
          <h5 class="title-primary">Fichas de Inscripción</h5>
          <p>Elige la comunidad a la cual deseas exportar su <strong>ficha de inscripción</strong>,
            y da clic en el botón de <b class="highlight">Exportar</b>.</p>
          <!-- <h3 class="text-center" style="line-height:1">FUEGO NUEVO 2017</h3> -->
        </div>

        <div class="row column">
          <form id="form-export">
            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="comunidades" class="text-right hide-for-small-only">Comunidad:</label>
                <label for="comunidades" class="show-for-small-only">Comunidad:</label>
              </div>

              <div class="large-10 medium-10 columns">
                <select id="comunidades" name="comunidad"></select>
              </div>
            </div>

            <div class="row">
              <div class="large-4 large-offset-8 columns end">
                <input type="submit" class="medium expanded button float-right" value="Exportar">
                <input name="accion" type="hidden" value="exportar-ficha-inscripcion">
              </div>
            </div>
          </form>
        </div>
      </div>
    </main>

    <?php require_once 'footer.php'; ?>

    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/what-input.js"></script>
    <script src="../js/vendor/foundation.min.js"></script>
    <script src="../js/app.js"></script>
    <script>
      $(document).ready(function() {
         var datos = [
          {
            id: 'comunidades',
            data: {
              accion: 'obtener-comunidades',
              modulo: 'export'
            },
            input: 'select'
          }
        ];
        var datosCargados = 0;

        // Cargar datos.
        (function() {
          mostrarLoading();

          datos.forEach(function(dato, index) {
            var id = dato.id;
            var name = dato.name;
            var data = dato.data;
            var input = dato.input;
            var dataset = dato.dataset;
            var selector = id ? '#' + id : '[name="' + name + '"]';

            $.post('../php/api.php', data, function(response) {
              if (response.status === 'OK') {
                var data = response.data;

                // Revisar a que tipo de input se le daran los valores.
                switch (input) {
                  case 'select':
                    for (var i = 0; i < data.length; i++) {
                      $(selector).append('<option value=' +
                        data[i].id + '>' +
                        data[i].nombre + '</option>');

                      if (dataset) {
                        dataset.forEach(function(key, index) {
                          $(selector).find(':last')
                            .data(key, data[i][key]);
                        });
                      }
                    }
                  break;
                }

                // Si tiene un dataset, mandar a llamar su evento onchange.
                // if (dataset) {
                //   $(selector).trigger('change');
                // }

                datosCargados += 1;
              } else {
                mostrarMensaje(response.msg);
              }

              // Ya termino de cargar todos los datos.
              if (datosCargados === datos.length) {
                ocultarLoading();
              }
            }, 'json').fail(function() {
              ocultarLoading();
              mostrarMensaje('Falló la conexión al servidor,' +
                ' por favor vuelve a intentarlo.');
            });
          });
        })();

        $('#form-export').on('submit', function(evt) {
          var formData = new FormData(this);

          // Mostrar loading.
          mostrarLoading();

          // Manadar request para evaluar y almacenar el archivo word.
          $.ajax({
            url: '../php/api.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(response) {
              if (response.status === 'OK') {
                window.open(response.data, '_blank');
              }

              ocultarLoading();
              mostrarMensaje(response.msg);
            },
            error: function(jqXHR, textStatus, errorThrown) {
              ocultarLoading();
              mostrarMensaje('Falló la conexión al servidor,' +
                ' por favor vuelve a intentarlo.');
            }
          });

          return evt.preventDefault();
        });

        $('body').removeClass('hide');
      });
    </script>
  </body>
</html>