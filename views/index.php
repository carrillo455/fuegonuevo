<?php
  session_start();
  if (!isset($_SESSION['usuario'])) {
    header('Location: ../index.php');
  } else {
    $claves = $_SESSION['usuario']['claves'];
    if (!in_array('SUD0', $claves) && !in_array('FN01', $claves)) {
      header('Location: ../index.php');
    }
  }
?>
<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pastoral Juvenil de la Diócesis de Tampico - Fuego Nuevo 2019</title>
    <link rel="shortcut icon" href="../favicon.png">
    <link rel="stylesheet" href="../css/app.css">
    <link rel="stylesheet" href="../css/foundation.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  </head>
  <body class="hide">
    <?php require_once 'header.php'; ?>

    <main>
      <div class="medium callout">
        <div class="row">
          <div class="large-12 columns">
            <h2 class="text-center">Formulario de Inscripción</h2>
            <!-- <h3 class="text-center" style="line-height:1">FUEGO NUEVO 2019</h3> -->
          </div>
        </div>

        <div class="row column">
          <div class="callout small">
            <h5 class="title-primary">Instrucciones</h5>
            <ol style="margin-left: 3rem;">
              <li>Llena todos los campos de la <b class="highlight">Información Base</b>,
                y da clic en el boton de <b class="highlight">Guardar Información Base</b>.
              </li>
              <li>Da clic en la pestaña de <b class="highlight">Inscripción</b>, y llena
                la información de tus integrantes <b>por disciplina</b>.<br>
                Puedes guardar en cualquier momento la información de tus integrantes
                dando clic en el boton de <b class="highlight">Guardar Inscripción</b>.
              </li>
              <li>Descarga las <b class="highlight">Credenciales Parroquiales</b>:
                <ul style="list-style-type: none">
                  <li>- Éstas las deberán llenar para presentarlas con su <u>información completa</u> el día de <strong>Fuego Nuevo 2019</strong>.</li>
                </ul>
              </li>
              <li>Descarga la ficha de <b class="highlight">Datos Médicos</b>:
                <ul style="list-style-type: none">
                  <li>- Ésta la deberán llenar <u>una por cada integrante</u> de tu(s) equipo(s), y las presentarán el día de <strong>Fuego Nuevo 2019</strong>.</li>
                </ul>
              </li>
            </ol>
            <div class="row">
              <div class="large-4 large-offset-2 medium-4 medium-offset-4 columns">
                <a id="descargar-credenciales-parroquiales" href="../download/fn2018-credenciales-parroquiales.pdf" target="_blank" class="medium secondary button expanded file-upload">Descargar Credenciales Parroquiales</a>
              </div>
              <div class="large-4 medium-4 columns end">
                <a id="descargar-datos-medicos" href="../download/fn2018-ficha-de-datos-medicos.pdf" target="_blank" class="medium secondary button expanded file-upload">Descargar Ficha de Datos Médicos</a>
              </div>
            </div>
            <p>
              * Puedes modificar tu <b class="highlight">Información Base</b> hasta antes del <strong>21/04/2019</strong>.
              <br>
              * Puedes modificar y complementar tu <b class="highlight">Inscripcion</b> hasta antes del <strong>21/04/2019</strong>.
            </p>
            <p class="lead text-center"><strong>Cualquier duda, porfavor acercate a un líder de la <b class="highlight">Pastoral Juvenil</b>.</strong></p>
          </div>
        </div>

        <div class="row column">
          <ul id="tabs-main" class="tabs" data-tabs>
            <li class="tabs-title is-active"><a href="#tab-informacion-base" aria-selected="true">Información Base</a></li>
            <li class="tabs-title"><a href="#tab-inscripcion">Inscripción</a></li>
          </ul>

          <div class="tabs-content" data-tabs-content="tabs-main">
            <div id="tab-informacion-base" class="tabs-panel is-active">
              <form id="form-informacion-base">
                <div class="row">
                  <div class="large-2 medium-2 columns">
                    <label for="comunidades" class="text-right hide-for-small-only">Comunidad</label>
                    <label for="comunidades" class="show-for-small-only">Comunidad</label>
                  </div>

                  <div class="large-10 medium-10 columns">
                    <select id="comunidades" name="comunidad"></select>
                  </div>
                </div>

                <!-- Datos del Coordinador -->
                <div class="row" data-coordinador>
                  <div class="large-12 columns">
                    <fieldset class="fieldset">
                      <legend>Datos del <strong>Coordinador</strong></legend>
                      <input name="coordinador[id]" type="hidden">

                      <div class="row">
                        <div class="large-2 medium-2 columns">
                          <label class="text-right hide-for-small-only">Nombre & Apellidos*</label>
                          <label class="show-for-small-only">Nombre & Apellidos*</label>
                        </div>

                        <div class="large-4 medium-10 columns">
                          <input name="coordinador[nombre]" type="text" placeholder="Nombre(s)" required>
                        </div>

                        <div class="large-3 medium-6 columns">
                          <input name="coordinador[apellido-paterno]" type="text" placeholder="Apellido Paterno" required>
                        </div>

                        <div class="large-3 medium-6 columns">
                          <input name="coordinador[apellido-materno]" type="text" placeholder="Apellido Materno" required>
                        </div>
                      </div>

                      <div class="row">
                        <div class="large-2 medium-2 columns">
                          <label class="text-right hide-for-small-only">Celular*</label>
                          <label class="show-for-small-only">Celular*</label>
                        </div>

                        <div class="large-4 medium-4 columns">
                          <input name="coordinador[celular]" type="text" placeholder="Celular" data-phone required>
                        </div>

                        <div class="large-1 medium-1 columns">
                          <label class="text-right hide-for-small-only">Email*</label>
                          <label class="show-for-small-only">Email*</label>
                        </div>

                        <div class="large-5 medium-5 columns">
                          <input name="coordinador[email]" type="text" placeholder="Email" required>
                        </div>
                      </div>

                      <div class="row">
                        <div class="large-2 medium-2 columns">
                          <label class="text-right hide-for-small-only">Fecha de Nacimiento*</label>
                          <label class="show-for-small-only">Fecha de Nacimiento*</label>
                        </div>

                        <div class="large-6 medium-6 small-8 columns">
                          <input name="coordinador[fecha-nacimiento]" type="text" placeholder="dd/mm/aaaa" data-date required>
                          <span class="form-error">
                            Favor de revisar la fecha de nacimiento, el formato adecuado es "<i>dd/mm/aaaa</i>".
                          </span>
                        </div>

                        <div class="large-1 medium-1 columns hide-for-small-only">
                          <label class="text-right">Edad</label>
                        </div>

                        <div class="large-3 medium-3 small-4 columns">
                          <input type="text" placeholder="Edad" data-age="coordinador" disabled>
                        </div>
                      </div>

                      <!-- <div class="row">
                        <div class="large-2 medium-2 columns">
                          <label for="es-participante-coordinador">¿Va a participar en la discilplina?</label>
                        </div>

                        <div class="large-4 medium-4 columns end">
                          <select name="es-participante-coordinador">
                            <option value="no">NO</option>
                            <option value="si">SI</option>
                          </select>
                        </div>
                      </div> -->
                    </fieldset>
                  </div>
                </div>
                <!-- -->

                <!-- Datos del Delegado -->
                <div class="row">
                  <div class="large-10 large-offset-2 medium-10 medium-offset-2 columns">
                    <input id="es-delegado" name="delegado[checked]" type="checkbox">
                    <label for="es-delegado">Delegado para asistir a junta</label>
                  </div>
                </div>

                <div class="row hide" data-delegado>
                  <div class="large-12 columns">
                    <fieldset class="fieldset">
                      <legend>Datos del <strong>Delegado</strong></legend>
                      <input name="delegado[id]" type="hidden">

                      <div class="row">
                        <div class="large-2 medium-2 columns">
                          <label class="text-right hide-for-small-only">Nombre & Apellidos</label>
                          <label class="show-for-small-only">Nombre & Apellidos</label>
                        </div>

                        <div class="large-4 medium-10 columns">
                          <input name="delegado[nombre]" type="text" placeholder="Nombre(s)">
                        </div>

                        <div class="large-3 medium-6 columns">
                          <input name="delegado[apellido-paterno]" type="text" placeholder="Apellido Paterno">
                        </div>

                        <div class="large-3 medium-6 columns">
                          <input name="delegado[apellido-materno]" type="text" placeholder="Apellido Materno">
                        </div>
                      </div>

                      <div class="row">
                        <div class="large-2 medium-2 columns">
                          <label class="text-right hide-for-small-only">Celular</label>
                          <label class="show-for-small-only">Celular</label>
                        </div>

                        <div class="large-4 medium-4 columns">
                          <input name="delegado[celular]" type="text" placeholder="Celular" data-phone>
                        </div>

                        <div class="large-1 medium-1 columns">
                          <label class="text-right hide-for-small-only">Email</label>
                          <label class="show-for-small-only">Email</label>
                        </div>

                        <div class="large-5 medium-5 columns">
                          <input name="delegado[email]" type="text" placeholder="Email">
                        </div>
                      </div>

                      <div class="row">
                        <div class="large-2 medium-2 columns">
                          <label class="text-right hide-for-small-only">Fecha de Nacimiento</label>
                          <label class="show-for-small-only">Fecha de Nacimiento</label>
                        </div>

                        <div class="large-6 medium-6 small-8 columns">
                          <input name="delegado[fecha-nacimiento]" type="text" placeholder="dd/mm/aaaa" data-date>
                          <span class="form-error">
                            Favor de revisar la fecha de nacimiento, el formato adecuado es "<i>dd/mm/aaaa</i>".
                          </span>
                        </div>

                        <div class="large-1 medium-1 columns hide-for-small-only">
                          <label class="text-right">Edad</label>
                        </div>

                        <div class="large-3 medium-3 small-4 columns">
                          <input type="text" placeholder="Edad" data-age="delegado" disabled>
                        </div>
                      </div>
                    </fieldset>
                  </div>
                </div>
                <!-- -->

                <!-- Carta del Sacerdote -->
                <div class="row">
                  <div class="large-2 medium-2 columns">
                    <label for="elegir-carta" class="text-right hide-for-small-only">Carta del Sacerdote*</label>
                    <label for="elegir-carta" class="show-for-small-only">Carta del Sacerdote*</label>
                  </div>

                  <div class="large-2 medium-2 columns">
                    <a id="elegir-carta" name="elegir-carta" class="small secondary button expanded file-upload">Elegir PDF o JPG
                      <input id="archivo-carta" name="archivo-carta" type="file" accept="application/pdf,image/jpeg" required>
                    </a>
                  </div>

                  <div class="large-8 medium-8 columns">
                    <small id="nombre-archivo-carta" name="nombre-archivo-carta">No se ha elegido archivo.</small>
                  </div>
                </div>
                <!-- -->

                <!-- Lista de Miembros -->
                <div class="row">
                  <div class="large-2 medium-2 columns">
                    <label for="elegir-lista" class="text-right hide-for-small-only">Lista de Miembros*</label>
                    <label for="elegir-lista" class="show-for-small-only">Lista de Miembros*</label>
                  </div>

                  <div class="large-2 medium-2 columns">
                    <a id="elegir-lista" name="elegir-lista" class="small secondary button expanded file-upload">Elegir PDF
                      <input id="archivo-lista" name="archivo-lista" type="file" accept="application/pdf" required>
                    </a>
                  </div>

                  <div class="large-8 medium-8 columns">
                    <small id="nombre-archivo-lista" name="nombre-archivo-lista">No se ha elegido archivo.</small>
                  </div>
                </div>
                <!-- -->

                <!-- <div class="row">
                  <div class="large-8 medium-8 columns">
                    <p>Favor de <u>descargar y llenar</u> las <b class="highlight">Credenciales Parroquiales</b>. Éstas las deberán presentar el día de <strong>Fuego Nuevo 2019</strong>.</p>
                  </div>

                  <div class="large-4 medium-4 columns end">
                    <a id="descargar-credenciales-parroquiales" href="../download/fn2018-credenciales-parroquiales.pdf" target="_blank" class="small secondary button expanded file-upload">Descargar credenciales parroquiales</a>
                  </div>
                </div> -->

                <div class="row">
                  <div class="large-6 large-offset-6 columns end">
                    <input type="submit" class="large expanded button float-right" value="Guardar Información Base">
                    <input name="accion" type="hidden" value="guardar-informacion-base">
                    <input id="informacion-base" name="informacion-base" type="hidden">
                  </div>
                </div>
              </form>
            </div>

            <div id="tab-inscripcion" class="tabs-panel">
              <form id="form-inscripcion">
                <div class="row">
                  <div class="large-2 medium-2 columns">
                    <label for="disciplinas" class="text-right hide-for-small-only">Disciplina</label>
                    <label for="disciplinas" class="show-for-small-only">Disciplina</label>
                  </div>

                  <div class="large-10 medium-10 columns">
                    <select id="disciplinas" name="disciplina" data-confirm="true"></select>
                  </div>
                </div>

                <div class="row">
                  <div class="large-12 columns">
                    <!-- <h3 class="text-center">Integrantes del Equipo</h3> -->
                    <h5 class="title-primary">Integrantes del Equipo</h5>
                    <ol style="margin-left: 3rem;">
                      <li>Da clic en el botón de <b class="highlight">Agregar Integrante</b> para añadir un intengrante más al equipo.</li>
                      <li>Considera que cada disciplina tiene <u>límites <b>mínimos</b> y <b>máximos</b></u> de integrantes.</li>
                      <li>Recomendamos <b class="highlight">Guardar Inscripción</b> cada vez que termines de llenar la información de un integrante, <b>para evitar pérdidas de información</b>.</li>
                    </ol>
                  </div>
                </div>

                <div class="row">
                  <div class="large-12 columns text-center">
                    <a id="agregar-integrante" class="medium button" href="#integrante">Agregar Integrante</a>
                  </div>
                </div>

                <div class="row">
                  <div class="large-12 columns">
                    <ul id="tabs-integrantes" class="tabs" data-tabs>
                      <li class="tabs-title is-active"><a href="#integrante-1" aria-selected="true">#1</a></li>
                    </ul>

                    <div id="tabs-content-integrantes" class="tabs-content" data-tabs-content="tabs-integrantes">
                      <div id="integrante-1" class="tabs-panel is-active" data-template="integrante">
                        <input name="integrantes[id][]" type="hidden">
                        <div class="row">
                          <div class="large-2 medium-2 columns">
                            <label class="text-right hide-for-small-only">Tipo de Integrante</label>
                            <label class="show-for-small-only">Tipo de Integrante</label>
                          </div>

                          <div class="large-10 medium-10 columns">
                            <select name="integrantes[tipo][]" data-default="0" data-attr="selected"></select>
                          </div>
                        </div>

                        <div class="row">
                          <div class="large-2 medium-2 columns">
                            <label class="text-right hide-for-small-only">Nombre & Apellidos</label>
                            <label class="show-for-small-only">Nombre & Apellidos</label>
                          </div>

                          <div class="large-4 medium-10 columns">
                            <input name="integrantes[nombre][]" type="text" placeholder="Nombre(s)">
                          </div>

                          <div class="large-3 medium-6 columns">
                            <input name="integrantes[apellido-paterno][]" type="text" placeholder="Apellido Paterno">
                          </div>

                          <div class="large-3 medium-6 columns">
                            <input name="integrantes[apellido-materno][]" type="text" placeholder="Apellido Materno">
                          </div>
                        </div>

                        <div class="row">
                          <div class="large-2 medium-2 columns">
                            <label class="text-right hide-for-small-only">Fecha de Nacimiento</label>
                            <label class="show-for-small-only">Fecha de Nacimiento</label>
                          </div>

                          <div class="large-6 medium-6 small-8 columns">
                            <input name="integrantes[fecha-nacimiento][]" type="text" placeholder="dd/mm/aaaa" data-date>
                            <span class="form-error">
                              Favor de revisar la fecha de nacimiento, el formato adecuado es "<i>dd/mm/aaaa</i>".
                            </span>
                          </div>

                          <div class="large-1 medium-1 columns hide-for-small-only">
                            <label class="text-right">Edad</label>
                          </div>

                          <div class="large-3 medium-3 small-4 columns">
                            <input type="text" placeholder="Edad" data-age="integrante" disabled>
                          </div>
                        </div>

                        <div class="row">
                          <div class="large-2 medium-2 columns">
                            <label class="text-right hide-for-small-only">Fotografía
                              <p><small>Da clic en la imágen para cambiar la imágen.</small></p>
                            </label>
                            <label class="show-for-small-only">Fotografía & Credencial INE
                              <p><small>Da clic en la imágen para cambiar la imágen.</small></p>
                              <p><small>La credencial puede ser: INE, Escolar, Credencial de Grupo o Licencia.</small></p>
                            </label>
                          </div>

                          <div class="large-4 medium-4 small-6 columns">
                            <a name="elegir-fotografia" class="text-center thumbnail" title="Da clic para elegir una imagen">
                              <img name="fotografia" src="../img/default-pic.jpg"
                                data-default="../img/default-pic.jpg" data-attr="src">
                            </a>
                            <input data-name="archivo-fotografia" type="file" accept="image/jpeg,image/png" hidden>
                          </div>

                          <div class="large-2 medium-2 columns hide-for-small-only">
                            <label class="text-right">Credencial
                              <p><small>* INE, Escolar, Credencial de Grupo o Licencia.</small></p>
                              <p><small>Da clic en la imágen para cambiar la imágen.</small></p>
                            </label>
                          </div>

                          <div class="large-4 medium-4 small-6 columns">
                            <!-- <a name="elegir-credencial" class="small button" title="Da clic para elegir una imagen"> -->
                            <a name="elegir-credencial" class="text-center thumbnail" title="Da clic para elegir una imagen">
                              <img name="credencial" src="../img/ine.jpg" data-default="../img/ine.jpg" data-attr="src"><!-- Elegir PDF Credencial -->
                            </a>
                            <p name="nombre-archivo-credencial" data-default="" data-attr="text"><!-- No se ha elegido PDF. --></p>
                            <input data-name="archivo-credencial" type="file" accept="image/jpeg,image/png" hidden>
                          </div>
                        </div>

                        <!-- <div class="row">
                          <div class="large-2 medium-2 columns">
                            <label for="datos-medicos" class="text-right hide-for-small-only">Datos Médicos*
                              <p><small>Adjunta la Ficha de Datos Médicos del integrante.</small></p>
                            </label>
                            <label for="datos-medicos" class="show-for-small-only">Datos Médicos*</label>
                          </div>

                          <div class="large-4 medium-4 columns">
                            <a id="elegir-datos-medicos" name="elegir-datos-medicos" class="small button expanded file-upload">Elegir Ficha de Datos Médicos
                              <input id="archivo-datos-medicos" name="archivo-datos-medicos" type="file" accept="application/pdf" required>
                            </a>
                          </div>

                          <div class="large-6 medium-6 columns">
                            <small id="nombre-archivo-datos-medicos" name="nombre-archivo-datos-medicos">No se ha elegido archivo.</small>
                          </div>
                        </div> -->
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row column">
                  <div class="callout alert small">
                    <h5>Recuerda que...</h5>
                    <p>Puedes guardar en cualquier momento la información de tus integrantes.</p>
                    <p>Solo recuerda completar TODA la información de los integrantes de tu equipo antes del
                    <strong>21/04/2019</strong>.<br>
                    En caso contrario, <strong>tu inscripción NO será válida</strong>.</p>
                  </div>
                </div>

                <div class="row">
                  <br>
                  <div class="large-6 large-offset-6 columns end">
                    <input type="submit" class="large expanded button float-right" value="Guardar Inscripción">
                    <input name="accion" type="hidden" value="guardar-inscripcion">
                    <input id="inscripcion" name="inscripcion" type="hidden">
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </main>

    <?php require_once 'footer.php'; ?>

    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/what-input.js"></script>
    <script src="../js/vendor/foundation.min.js"></script>
    <script src="../js/vendor/jquery.mask.min.js"></script>
    <script src="../js/app.js"></script>
    <script>
      $(document).ready(function() {
        var datos = [
          {
            id: 'comunidades',
            accion: 'obtener-comunidades',
            input: 'select'
          },
          {
            id: 'disciplinas',
            accion: 'obtener-disciplinas',
            input: 'select',
            dataset: ['min', 'max']
          },
          {
            name: 'integrantes[tipo][]',
            accion: 'obtener-tipos-integrantes',
            input: 'select',
            dataset: ['min', 'max']
          }
        ];
        var datosCargados = 0;
        var maskOptions = {
          date: {
            onComplete: function(cep) {
              var target = window.event.target;
              var parent = $(target).closest('.row');
              var dateInput = $(parent).find('[data-date]');
              var ageInput = $(parent).find('[data-age]');
              var name = dateInput.attr('name');
              var edad = calcularEdad(cep);
              if (!edad) {
                ageInput.val('');
                return;
              }

              if (name.match(/integrantes/) && (edad < 15 || edad > 30)) {
                dateInput.val(dateInput.val().substring(0,9));
                return mostrarMensaje('Los participantes deben de tener de' +
                  ' 15 a 30 años cumplidos.');
              }

              ageInput.val(edad);
              dateInput.removeClass('is-invalid-input');
              $(parent).find('.form-error').removeClass('is-visible');
            },
            onChange: function(cep) {
              var target = window.event.target;
              var parent = $(target).closest('.row');
              $(parent).find('[data-age]').val('');
              $(parent).find('[data-date]').addClass('is-invalid-input');
              $(parent).find('.form-error').addClass('is-visible');
            }
          }
        };
        var calcularEdad = function(rawDate) {
          if (rawDate === null) return '';
          var formattedDate = rawDate.split('/').reverse().join('-');
          var birthDate = new Date(formattedDate);
          var diffDate = new Date - birthDate;
          var ageDate = new Date(diffDate);
          ageDate.setHours(ageDate.getHours() - 24); // Hack.. no era exacto.
          var age = Math.abs(ageDate.getUTCFullYear() - 1970);
          if (isNaN(age)) {
            return false;
          }

          return age;
        };
        var crearTabIntegrante = function(closeable, focusOnNew) {
          var max = parseInt($('#disciplinas option:selected').data('max'));
          if ($('#tabs-integrantes').children().length >= max) {
            mostrarMensaje('Has llegado al límite de <strong>' + max + '</strong>' +
              ' integrantes por la disciplina de <strong>' +
              $('#disciplinas option:selected').text() + '</strong>.');
            return;
          }

          if ($('#tabs-integrantes').children().length > 10
            && ( $('#inscripcion').val() === '' || $('#inscripcion').val() === 'false')) {
            mostrarMensaje('¡Espera! Antes de seguir agregando mas integrantes' +
              ' es necesario que <strong>guardes</strong> para evitar ' +
              ' pérdida de información. Por favor, da clic en el boton ' +
              ' <strong class="highlight">Guardar Inscripción</strong>');
            return;
          }

          var tabs = $('#tabs-integrantes');
          var tabsContent = $('#tabs-content-integrantes');
          var template = $('[data-template="integrante"]').clone(true, false);

          // Crear la nueva tab.
          var tab = document.createElement('li');
          var tabAnchor = document.createElement('a');
          var tabIndex = tabs.children().length + 1;
          tab.classList.add('tabs-title');
          tabAnchor.href = '#integrante-' + tabIndex;
          tabAnchor.textContent = '#' +  tabIndex;
          tab.appendChild(tabAnchor);
          tabs.append(tab);

          // Limpiar el template.
          template.attr('id', 'integrante-' + tabIndex);
          template.removeClass('is-active');
          template.removeAttr('data-template');
          template.find(':input').val('');
          template.find('.is-invalid-input').removeClass('is-invalid-input');
          template.find('.is-visible').removeClass('is-visible');

          // Asignar valores por defecto.
          $.each(template.find('[data-default]'), function(i, elem) {
            switch (elem.dataset.attr) {
              case 'src':
                elem.src = elem.dataset.default;
              break;

              case 'text':
                elem.textContent = elem.dataset.default;
              break;

              case 'selected':
                elem.options.selectedIndex = elem.dataset.default;
              break;
            }
          });

          // Asignar eventos.
          template.find('[data-date]')
            .unmask()
            .mask('00/00/0000', maskOptions.date);

          // Insertar la tachita.
          template.css('position', 'relative');
          template.append('<button class="close-button cb-1" type="button"' +
            (closeable ? "" : " hidden") + '>' +
            '<span aria-hidden="true">&times;</span>' +
          '</button>');
          template.find('.cb-1').on('click', function(evt) {
            var id = $(this).closest('.tabs-panel').attr('id');
            $('#tabs-integrantes').find('[href="#'+id+'"]')
              .parent().prev().trigger('click')
              .end().remove();
            $('#tabs-content-integrantes').find('#'+id).remove();
          });

          // Insertar el template en el DOM.
          tabsContent.append(template);
          Foundation.reInit('tabs');
          if (focusOnNew) {
            tabAnchor.click(); // Dar click en el tab agregado.
          }
        };
        var limpiarTabsIntegrantes = function() {
          $('#inscripcion').val('');
          $.each($('#tabs-content-integrantes').children(),
            function(i, content) {
              if (content.dataset.template) {
                return $(content)
                  .find('select')
                  .each(function(i, select) {
                    select.options.selectedIndex = select.dataset.default;
                  })
                  .end()
                  .find('input')
                  .val('')
                  .end()
                  .find('img')
                  .each(function(i, img) {
                    img.src = img.dataset.default;
                  });
              }

              $(content).find('.cb-1').trigger('click');
              return;
            });
        };

        // Cargar datos.
        (function() {
          mostrarLoading();

          datos.forEach(function(dato, index) {
            var id = dato.id;
            var name = dato.name;
            var accion = dato.accion;
            var input = dato.input;
            var dataset = dato.dataset;
            var selector = id ? '#' + id : '[name="' + name + '"]';

            $.post('../php/api.php', {
              accion: accion
            }, function(response) {
              if (response.status === 'OK') {
                var data = response.data;

                // Revisar a que tipo de input se le daran los valores.
                switch (input) {
                  case 'select':
                    for (var i = 0; i < data.length; i++) {
                      $(selector).append('<option value=' +
                        data[i].id + '>' +
                        data[i].nombre + '</option>');

                      if (dataset) {
                        dataset.forEach(function(key, index) {
                          $(selector).find(':last')
                            .data(key, data[i][key]);
                        });
                      }
                    }
                  break;

                  case 'tabs':
                    for (var i = 0; i < data.length; i++) {
                      $('#agregar-integrante').trigger('click');
                      $('[name="nombre[]"]:last').val(data[i].nombre);
                      $('[name="apellido-paterno[]"]:last').val(data[i].apellidoPaterno);
                      $('[name="apellido-materno[]"]:last').val(data[i].apellidoMaterno);
                      $('[name="fecha-nacimiento[]"]:last').val(data[i].fechaNacimiento);
                      $('[name="nombre[]"]:last').val(data[i].nombre);
                    }
                  break;
                }

                // Si tiene un dataset, mandar a llamar su evento onchange.
                // if (dataset) {
                //   $(selector).trigger('change');
                // }

                datosCargados += 1;
              } else {
                mostrarMensaje(response.msg);
              }

              // Ya termino de cargar todos los datos.
              if (datosCargados === datos.length) {
                $.post('../php/api.php', {
                  accion: 'obtener-informacion-base',
                  comunidad: $('#comunidades').val()
                }, function(response) {
                  if (response.status === 'OK') {
                    if (!response.data) {
                      ocultarLoading();
                      $('#disciplinas').trigger('change');
                      return;
                    }

                    var data = response.data;
                    var informacionBase = data.informacionBase;
                    var coordinador = data.coordinador;
                    var delegado = data.delegado;
                    var sinDelegado = ('' === delegado.nombre)
                      && ('' === delegado.apellidoPaterno)
                      && ('' === delegado.apellidoMaterno);

                    $('[name="coordinador[id]"]').val(coordinador.id);
                    $('[name="coordinador[nombre]"]').val(coordinador.nombre);
                    $('[name="coordinador[apellido-paterno]"]').val(coordinador.apellidoPaterno);
                    $('[name="coordinador[apellido-materno]"]').val(coordinador.apellidoMaterno);
                    $('[name="coordinador[celular]"]').val(coordinador.celular);
                    $('[name="coordinador[email]"]').val(coordinador.email);
                    $('[name="coordinador[fecha-nacimiento]"]').val(coordinador.fechaNacimiento);
                    $('[data-age="coordinador"]').val(calcularEdad(coordinador.fechaNacimiento));

                    if (!sinDelegado) {
                      $('#es-delegado').attr('checked', true);
                      $('#es-delegado').trigger('change');
                    }

                    $('[name="delegado[id]"]').val(delegado.id);
                    $('[name="delegado[nombre]"]').val(delegado.nombre);
                    $('[name="delegado[apellido-paterno]"]').val(delegado.apellidoPaterno);
                    $('[name="delegado[apellido-materno]"]').val(delegado.apellidoMaterno);
                    $('[name="delegado[celular]"]').val(delegado.celular);
                    $('[name="delegado[email]"]').val(delegado.email);
                    $('[name="delegado[fecha-nacimiento]"]').val(delegado.fechaNacimiento);
                    $('[data-age="delegado"]').val(calcularEdad(delegado.fechaNacimiento));

                    if (informacionBase.carta !== null) {
                      $('#nombre-archivo-carta').html('')
                        .parent().append('<a href="'+informacionBase.carta+
                        '" target="_blank" class="file-uploaded">Aquí esta la '+
                        '<strong>CARTA DEL SACERDOTE</strong> que subíste, ¡da click!'+
                        '<input name="url-carta" type="hidden" value="'+
                        informacionBase.carta+'">' +
                        '</a>');

                      $('#archivo-carta').get(0).removeAttribute('required');
                    }

                    if (informacionBase.lista !== null) {
                      $('#nombre-archivo-lista').html('')
                        .parent().append('<a href="'+informacionBase.lista+
                        '" target="_blank" class="file-uploaded">Aquí esta la '+
                        '<strong>LISTA DE MIEMBROS</strong> que subíste, ¡da click!'+
                        '<input name="url-lista" type="hidden" value="'+
                        informacionBase.lista+'">' +
                        '</a>');

                      $('#archivo-lista').get(0).removeAttribute('required');
                    }

                    $('#informacion-base').val(informacionBase.id);
                    $('#disciplinas').trigger('change');
                  } else {
                    mostrarMensaje(response.msg);
                    ocultarLoading();
                  }
                }, 'json').fail(function() {
                  ocultarLoading();
                  mostrarMensaje('Falló la conexión al servidor,' +
                    ' por favor vuelve a intentarlo.');
                });

                $('#informacion-base').find(':input:first').focus();
              }
            }, 'json').fail(function() {
              ocultarLoading();
              mostrarMensaje('Falló la conexión al servidor,' +
                ' por favor vuelve a intentarlo.');
            });
          });
        })();

        /*
        * ASIGNAR EVENTOS
        */
        // $('[data-coordinador] input').on('keyup', function() {
        //   var name = this.name;
        //   var value = this.value;
        //   var inputNames = ['nombre', 'apellido-paterno', 'apellido-materno',
        //     'fecha-nacimiento'];
        //   var selectCoordinador = $('#tabs-content-integrantes')
        //     .find('[name="integrantes[tipo][]"]')
        //     .find('option[value=1]:selected');
        //   var tabsPanelCoordinador = selectCoordinador.length === 0
        //     ? false
        //     : selectCoordinador.closest('.tabs-panel');

        //   if (tabsPanelCoordinador) {
        //     inputNames.forEach(function(inputName, i) {
        //       if (name.match(inputName) !== null) {
        //         tabsPanelCoordinador.find('[name="integrantes['+inputName+'][]"]')
        //           .val(value);
        //       }

        //       return;
        //     });
        //   }
        // });

        $('#tabs-main').on('click', function(evt) {
          return evt.preventDefault();
        });

        $('#es-delegado').on('change', function() {
          $('[data-delegado]').toggleClass('hide');
        });

        $('#disciplinas').on('click', function(evt) {
          this.dataset.selectedIndex = this.options.selectedIndex;
        });

        $('#disciplinas').on('change', function(evt) {
          var min = parseInt($(this).find('option:selected').data('min'));
          if (this.dataset.confirm === 'true') {
            var contenidoIntegrantes = $('#tabs-content-integrantes').find('input')
              .map(function(){return $(this).val();}).get().join('');
            var confirmar = contenidoIntegrantes === ''
              ? true
              : confirm('¡Cuidado! Vas a cambiar de disciplina, y la' +
              ' información no guardada de tus integrantes se borrará.\n\n' +
              '¿Deseas continuar?');

            if (!confirmar) {
              return this.options.selectedIndex = this.dataset.selectedIndex;
            }
          }

          this.dataset.confirm = 'true';
          limpiarTabsIntegrantes();
          for (var i = 1; i < min; i++) crearTabIntegrante(false, false);
          if ($('#informacion-base').val() === '') return;

          // Obtener integrantes.
          mostrarLoading();
          $.post('../php/api.php', {
            accion: 'obtener-inscripcion',
            'informacion-base': $('#informacion-base').val(),
            disciplina: this.value
          }, function(response) {
            if (response.status === 'OK') {
              var inscripcion = response.data;
              $('#inscripcion').val(inscripcion);

              $.post('../php/api.php', {
                accion: 'obtener-integrantes',
                inscripcion: inscripcion
              }, function(response) {
                if (response.status === 'OK') {
                  if (response.data.length === 0) {
                    ocultarLoading();
                    return;
                  }

                  var integrantes = response.data;
                  var length = integrantes.length;

                  for (var i = 0; i < length; i++) {
                    var tabContent = $('#integrante-' + (i+1));
                    if (tabContent.length === 0) {
                      crearTabIntegrante(true, false);
                      tabContent = $('#integrante-' + (i+1));
                    }

                    tabContent.find('[name="integrantes[id][]"]').val(integrantes[i].id);
                    tabContent.find('[name="integrantes[tipo][]"]').val(integrantes[i].tipo);
                    tabContent.find('[name="integrantes[nombre][]"]').val(integrantes[i].nombre);
                    tabContent.find('[name="integrantes[apellido-paterno][]"]').val(integrantes[i].apellidoPaterno);
                    tabContent.find('[name="integrantes[apellido-materno][]"]').val(integrantes[i].apellidoMaterno);
                    tabContent.find('[name="integrantes[fecha-nacimiento][]"]').val(integrantes[i].fechaNacimiento);
                    tabContent.find('[data-age="integrante"]').val(calcularEdad(integrantes[i].fechaNacimiento));

                    if (integrantes[i].fotografia !== null) {
                      tabContent.find('[name="fotografia"]').attr('src', integrantes[i].fotografia);
                      // tabContent.find('[name="fotografia"]').attr('data-default', integrantes[i].fotografia);
                    }

                    if (integrantes[i].credencial !== null) {
                      tabContent.find('[name="credencial"]').attr('src', integrantes[i].credencial);
                      // tabContent.find('[name="credencial"]').attr('data-default', integrantes[i].credencial);
                    }
                  }
                } else {
                  mostrarMensaje(response.msg);
                }

                ocultarLoading();
              }, 'json').fail(function() {
                ocultarLoading();
                mostrarMensaje('Falló la conexión al servidor,' +
                  ' por favor vuelve a intentarlo.');
              });
            } else {
              ocultarLoading();
              mostrarMensaje(response.msg);
            }
          }, 'json').fail(function() {
            ocultarLoading();
            mostrarMensaje('Falló la conexión al servidor,' +
              ' por favor vuelve a intentarlo.');
          });
        });

        $('#elegir-carta').on('click', function() {
          // $('#archivo-carta').trigger('click');
        });

        $('#archivo-carta').on('change', function() {
          if (this.files.length === 0) {
            $('#nombre-archivo-carta').html('No se ha elegido archivo.');
            return;
          }

          $('#nombre-archivo-carta').html(this.files[0].name);
          this.setAttribute('required', true);
        });

        $('#elegir-lista').on('click', function() {
          // $('#archivo-lista').trigger('click');
        });

        $('#archivo-lista').on('change', function() {
          if (this.files.length === 0) {
            $('#nombre-archivo-lista').html('No se ha elegido archivo.');
            return;
          }

          $('#nombre-archivo-lista').html(this.files[0].name);
          this.setAttribute('required', true);
        });

        $('#elegir-datos-medicos').on('click', function() {
          // $('#archivo-datos-medicos').trigger('click');
        });

        $('#archivo-datos-medicos').on('change', function() {
          if (this.files.length === 0) {
            $('#nombre-archivo-datos-medicos').html('No se ha elegido archivo.');
            return;
          }

          $('#nombre-archivo-datos-medicos').html(this.files[0].name);
        });

        $('#agregar-integrante').on('click', function() {
          return crearTabIntegrante(true, true);
        });

        $('[data-template="integrante"]').on('click', '[name="integrantes[tipo][]"]', function(evt) {
          this.dataset.selectedIndex = this.options.selectedIndex;
        });

        $('[data-template="integrante"]').on('change', '[name="integrantes[tipo][]"]', function(evt) {
          var value = this.value;
          var tabsContent = $('#tabs-content-integrantes');
          var tabsPanelInactive = tabsContent.children(':not(.is-active)');
          var max = 0;
          var count = 0;

          // Encontrar el max del tipo de integrante:
          // Hay que ir al primer select ya que ese contiene los min y max,
          // los clones no contienen esa informacion.
          $.each($('[name="integrantes[tipo][]"]:first option'), function() {
            if (value === this.value) {
              max = parseInt($(this).data('max'));
            }

            return;
          });

          $.each(tabsPanelInactive.find('[name="integrantes[tipo][]"]'), function(i, content) {
            if (value === this.value) {
              count += 1;
            }

            return;
          });

          if (count >= max) {
            mostrarMensaje('Has llegado al límite de <strong>' + max + '</strong> <i>' +
              $(this).find('option:selected').text() + '</i> por la disciplina de <strong>' +
              $('#disciplinas option:selected').text() + '</strong>.');

            return this.options.selectedIndex = this.dataset.selectedIndex;
          }
        });

        $('[data-template="integrante"]').on('click', '[name="elegir-fotografia"]', function() {
          $(this).parent().find('[data-name="archivo-fotografia"]').trigger('click');
        });

        $('[data-template="integrante"]').on('click', '[name="elegir-credencial"]', function() {
          $(this).parent().find('[data-name="archivo-credencial"]').trigger('click');
        });

        $('[data-template="integrante"]').on('change', '[data-name="archivo-fotografia"]', function() {
          if (this.files.length === 0) {
            return;
          }

          var parent = $(this).parent();
          var canvas = document.createElement('canvas');
          var img = new Image;
          var width = 280;
          var height = 250;

          canvas.width = width;
          canvas.height = height;
          img.onload = function() {
              canvas.getContext('2d').drawImage(img, 0, 0, width, height);
              data = canvas.toDataURL('image/jpeg', 0.75);
              parent.find('[name="fotografia"]').attr('src', data);
          };
          img.src = URL.createObjectURL(this.files[0]);
        });

        // $('[data-template="integrante"]').on('change', '[data-name="archivo-credencial"]', function() {
        //   var parent = $(this).parent();
        //   if (this.files.length === 0) {
        //     parent.find('[name="nombre-archivo-credencial"]').html('No se ha elegido PDF.');
        //     return;
        //   }

        //   parent.find('[name="nombre-archivo-credencial"]').html(this.files[0].name);
        // });

        $('[data-template="integrante"]').on('change', '[data-name="archivo-credencial"]', function() {
          if (this.files.length === 0) {
            return;
          }

          var parent = $(this).parent();
          var canvas = document.createElement('canvas');
          var img = new Image;
          var width = 720;
          var height = 450;

          canvas.width = width;
          canvas.height = height;
          img.onload = function() {
              canvas.getContext('2d').drawImage(img, 0, 0, width, height);
              data = canvas.toDataURL('image/jpeg', 0.75);
              parent.find('[name="credencial"]').attr('src', data);
          };
          img.src = URL.createObjectURL(this.files[0]);
        });

        $('#form-informacion-base').on('submit', function(evt) {
          // var emptyRequired = false;
          // // Quitamos clases de error.
          // $('.is-invalid-input').removeClass('is-invalid-input');
          // $('.form-error').removeClass('is-visible');

          // $.each($('[required]'), function(i, element) {
          //   if (element.value === '') {
          //     element.classList.add('is-invalid-input');
          //     emptyRequired = true;
          //   }
          // });

          // if (emptyRequired) {
          //   $('.is-invalid-input:first').focus();
          //   return evt.preventDefault();
          // }

          // Todo correcto, mandamos datos.
          $('#confirmar').find('p').html('Estás a punto de enviar la <strong>Información' +
            ' Básica</strong> para tener derecho a inscribir tu equipo en Fuego Nuevo 2019.<br><br>' +
            '¿Deseas continuar?');
          $('#confirmar').foundation('open');
          this.dataset.submitted = true;

          return evt.preventDefault();
        });

        $('#form-inscripcion').on('submit', function(evt) {
          // var emptyRequired = false;
          // // Quitamos clases de error.
          // $('.is-invalid-input').removeClass('is-invalid-input');
          // $('.form-error').removeClass('is-visible');

          // $.each($('[required]'), function(i, element) {
          //   if (element.value === '') {
          //     element.classList.add('is-invalid-input');
          //     emptyRequired = true;
          //   }
          // });

          // if (emptyRequired) {
          //   $('.is-invalid-input:first').focus();
          //   return evt.preventDefault();
          // }

          // Todo correcto, mandamos datos.
          $('#confirmar').find('p').html('Estás a punto de enviar la información' +
            ' para inscribir a tu equipo en Fuego Nuevo 2019.<br><br>' +
            '¿Deseas continuar?');
          $('#confirmar').foundation('open');
          this.dataset.submitted = true;

          return evt.preventDefault();
        });

        // Evento para deshabilitar el 'enter' para que no se ejecute
        // el submit del formulario.
        $(document).on('keydown', function(evt) {
          if (evt.keyCode === 13) {
            return evt.preventDefault();
          }
        });
        /*  */

        $('#confirmar').on('closed.zf.reveal', function(evt) {
          $('form[data-submitted="true"]').attr('data-submitted', false);
        });

        $('#confirmar').find('[data-accept]').on('click', function() {
          // Esta es la parte mas sabrosa del proceso.
          var form = $('form[data-submitted="true"]').get(0);
          var formId = form.id;
          var formData = new FormData(form);

          switch (formId) {
            case 'form-inscripcion':
              formData.append('comunidad', $('#comunidades').val());
              // Ingresamos las fotografias.
              $.each($('[name="fotografia"]'), function(i, img) {
                formData.append('integrantes[fotografia][]',
                  img.getAttribute('src') === img.dataset.default ? 'default' : img.src);
              });

              // Ingresamos las credenciales.
              $.each($('[name="credencial"]'), function(i, img) {
                formData.append('integrantes[credencial][]',
                  img.getAttribute('src') === img.dataset.default ? 'default' : img.src);
              });
            break;
          }

          // Mostrar loading.
          mostrarLoading();

          // Manadar request para evaluar y almacenar el archivo word.
          $.ajax({
            url: '../php/api.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(response) {
              if (response.status === 'OK') {
                // $('#'+formId.replace('form-', '')).val(response.data);
                var data = response.data;
                for (var key in data) {
                  $('[name="'+key+'"]').val(data[key]);
                }

                if (response.reload) {
                  $('#disciplinas').attr('data-confirm', false).trigger('change');
                }
              } else if (response.status === 'LACK') {

              }

              ocultarLoading();
              mostrarMensaje(response.msg);
            },
            error: function(jqXHR, textStatus, errorThrown) {
              ocultarLoading();
              mostrarMensaje('Falló la conexión al servidor,' +
                ' por favor vuelve a intentarlo.');
            }
          });
        });

        $('#archivos').on('change',function() {
          var confirmar = $('#confirmar');
          if (this.files.length > 0) {
            $.each(this.files, function(key, value) {
              confirmar.find('p.lead').html('¿Estás seguro de subir las' +
                ' <strong>ordenes de servicio</strong> contenidas' +
                ' en el archivo: <i>' + value.name + "</i>?");
            });

            confirmar.foundation('open');
          }
        });

        // Evento para agregar mascara a la fecha de nacimiento.
        $('[data-date]').mask('00/00/0000', maskOptions.date);
        $('[data-phone]').mask('0000000000');

        $('body').removeClass('hide');
      });
    </script>
  </body>
</html>