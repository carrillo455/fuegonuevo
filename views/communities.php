<?php
  session_start();
  if (!isset($_SESSION['usuario'])) {
    header('Location: ../index.php');
  } else {
    $claves = $_SESSION['usuario']['claves'];
    if (!in_array('SUD0', $claves) && !in_array('ADM02', $claves)) {
      header('Location: ../index.php');
    }
  }
?>
<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pastoral Juvenil de la Diócesis de Tampico - Fuego Nuevo 2019</title>
    <link rel="shortcut icon" href="../favicon.png">
    <link rel="stylesheet" href="../css/foundation.min.css">
    <link rel="stylesheet" href="../css/jquery.dataTables.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- <link rel="stylesheet" href="../css/buttons.dataTables.min.css"> -->
    <link rel="stylesheet" href="../css/app.css">
  </head>
  <body class="hide">
    <?php require_once 'header.php'; ?>

    <main>
      <div class="medium callout">
        <div class="row column">
          <h2 class="text-center">Comunidades</h2>
          <h5 class="title-primary">Nueva Comunidad</h5>
          <!-- <h3 class="text-center" style="line-height:1">FUEGO NUEVO 2017</h3> -->
        </div>

        <div class="row column">
          <form id="form-comunidad">
            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="templo" class="text-right hide-for-small-only">Nombre del Templo:</label>
                <label for="templo" class="show-for-small-only">Nombre del Templo:</label>
              </div>

              <div class="large-10 medium-10 columns">
                <input id="templo" name="nombre-templo" type="text" placeholder="Nombre del Templo" required>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="tipos-templos" class="text-right hide-for-small-only">Tipo de Templo:</label>
                <label for="tipos-templos" class="show-for-small-only">Tipo de Templo:</label>
              </div>

              <div class="large-10 medium-10 columns">
                <select id="tipos-templos" name="tipo-templo"></select>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="comunidad" class="text-right hide-for-small-only">Nombre de la(s) Comunidad(es):</label>
                <label for="comunidad" class="show-for-small-only">Nombre de la(s) Comunidad(es):</label>
              </div>

              <div class="large-10 medium-10 columns">
                <small>Si son comunidades <i>en conjunto</i> favor de separar con una "coma" una de otra
                  (p.e. <strong>Grupo 1, Grupo 2, Grupo 3</strong>).
                </small>
                <input id="comunidad" name="nombre-comunidad" type="text" placeholder="Nombre de la(s) Comunidad(es)" required>
              </div>
            </div>

            <div class="row">
              <div class="large-2 medium-2 columns">
                <label for="tipos-comunidades" class="text-right hide-for-small-only">Tipo de Comunidad:</label>
                <label for="tipos-comunidades" class="show-for-small-only">Tipo de Comunidad:</label>
              </div>

              <div class="large-10 medium-10 columns">
                <select id="tipos-comunidades" name="tipo-comunidad"></select>
              </div>
            </div>

            <div class="row">
              <div class="large-4 large-offset-8 columns end">
                <input type="submit" class="medium expanded button float-right" value="Guardar Comunidad(es)">
                <input name="accion" type="hidden" value="guardar-comunidad">
              </div>
            </div>
          </form>
        </div>

        <div class="row column">
          <h5 class="title-primary">Listado de Comunidades</h5>
        </div>

        <div class="row column">
          <table id="dt-comunidades" class="dataTable" data-init="false">
            <thead></thead>
          </table>
        </div>
      </div>
    </main>

    <?php require_once 'footer.php'; ?>

    <script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/what-input.js"></script>
    <script src="../js/vendor/foundation.min.js"></script>
    <script src="../js/vendor/jquery.dataTables.min.js"></script>
    <!-- <script src="../js/vendor/dataTables.buttons.min.js"></script>
    <script src="../js/vendor/dataTables.buttons.flash.min.js"></script> -->
    <script src="../js/app.js"></script>
    <script>
      $(document).ready(function() {
         var datos = [
          {
            id: 'tipos-templos',
            accion: 'obtener-tipos-templos',
            input: 'select'
          },
          {
            id: 'tipos-comunidades',
            accion: 'obtener-tipos-comunidades',
            input: 'select'
          }
        ];
        var datosCargados = 0;
        var columns = [
          {'title': '#', 'className': 'text-right', 'width': '1%'},
          {'title': '', 'className': 'td-id', 'visible': false},
          {'title': 'TEMPLO', 'width': '5%'},
          {'title': 'COMUNIDAD(ES)', 'width': '5%'},
          {'title': 'TIPO DE COMUNIDAD', 'width': '5%'},
          {'title': 'ACCIONES', 'className': 'text-center', 'width': '5%'}
        ];
        var table = $('#dt-comunidades').dataTable( {
          'language': {
            'url': '../json/datatables.spanish.lang.json'
          },
          'autoWidth': false,
          'scrollX': true,
          'pageLength': 25,
          'processing': true,
          'serverSide': true,
          'ajax': '../php/scripts/server_processing.php?o=comunidades',
          'columns': columns,
          'order': [],
          "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "Todos"] ],
          'initComplete': function( settings, json ) {
            var api = this.api();
            table.attr('data-init', true);

            table.on('click', '[data-delete]', function() {
              mostrarLoading();
              var id = this.dataset.delete;

              $.post('../php/api.php', {
                accion: 'borrar-comunidad',
                id: id
              }, function(response) {
                if (response.status === 'OK') {
                  table.DataTable().draw();
                }

                ocultarLoading();
                mostrarMensaje(response.msg);
              }, 'json').fail(function() {
                ocultarLoading();
                mostrarMensaje('Falló la conexión al servidor,' +
                  ' por favor vuelve a intentarlo.');
              });
            });

            table.on('click', '[data-undelete]', function() {
              mostrarLoading();
              var id = this.dataset.undelete;

              $.post('../php/api.php', {
                accion: 'incorporar-comunidad',
                id: id
              }, function(response) {
                if (response.status === 'OK') {
                  table.DataTable().draw();
                }

                ocultarLoading();
                mostrarMensaje(response.msg);
              }, 'json').fail(function() {
                ocultarLoading();
                mostrarMensaje('Falló la conexión al servidor,' +
                  ' por favor vuelve a intentarlo.');
              });
            });
          },
          'drawCallback': function( settings ) {
            var api = this.api();
            var start = settings._iDisplayStart;

            // Crear un consecutivo en la primera columna.
            api.column(0).nodes().each( function (cell, i) {
              start += 1;
              cell.innerHTML = start;
            });

            // Darle funcionalidad al dropdown proveniente del server-side.
            table.find('.dropdown').foundation();
          },
          'destroy': true
        });
        var limpiarFormulario = function() {
          return $('#form-comunidad')
            .find('select:not([data-default]) option:nth-child(1)')
            .prop('selected', true)
            .end()
            .find(':input')
            .not('select')
            // .not(':disabled')
            .not(':submit')
            .not('[name=accion]')
            .val('');
        };

        // Cargar datos.
        (function() {
          mostrarLoading();

          datos.forEach(function(dato, index) {
            var id = dato.id;
            var name = dato.name;
            var accion = dato.accion;
            var input = dato.input;
            var dataset = dato.dataset;
            var selector = id ? '#' + id : '[name="' + name + '"]';

            $.post('../php/api.php', {
              accion: accion
            }, function(response) {
              if (response.status === 'OK') {
                var data = response.data;

                // Revisar a que tipo de input se le daran los valores.
                switch (input) {
                  case 'select':
                    for (var i = 0; i < data.length; i++) {
                      $(selector).append('<option value=' +
                        data[i].id + '>' +
                        data[i].nombre + '</option>');

                      if (dataset) {
                        dataset.forEach(function(key, index) {
                          $(selector).find(':last')
                            .data(key, data[i][key]);
                        });
                      }
                    }
                  break;
                }

                // Si tiene un dataset, mandar a llamar su evento onchange.
                // if (dataset) {
                //   $(selector).trigger('change');
                // }

                datosCargados += 1;
              } else {
                mostrarMensaje(response.msg);
              }

              // Ya termino de cargar todos los datos.
              if (datosCargados === datos.length) {
                ocultarLoading();
              }
            }, 'json').fail(function() {
              ocultarLoading();
              mostrarMensaje('Falló la conexión al servidor,' +
                ' por favor vuelve a intentarlo.');
            });
          });
        })();

        $('#form-comunidad').on('submit', function(evt) {
          // Todo correcto, mandamos datos.
          $('#confirmar').find('p').html('Estás a punto de crear una comunidad o comunidades ' +
            '<strong>' + $('#comunidad').val() + '</strong> del templo de <strong>' +
            $('#templo').val() + '</strong>.<br><br>' +
            '¿Deseas continuar?');
          $('#confirmar').foundation('open');

          return evt.preventDefault();
        });

        $('#confirmar').find('[data-accept]').on('click', function() {
          // Esta es la parte mas sabrosa del proceso.
          var form = $('#form-comunidad').get(0);
          var formData = new FormData(form);

          // Mostrar loading.
          mostrarLoading();

          // Manadar request para evaluar y almacenar el archivo word.
          $.ajax({
            url: '../php/api.php',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(response) {
              if (response.status === 'OK') {
                table.DataTable().draw();
                limpiarFormulario();
              }

              ocultarLoading();
              mostrarMensaje(response.msg);
            },
            error: function(jqXHR, textStatus, errorThrown) {
              ocultarLoading();
              mostrarMensaje('Falló la conexión al servidor,' +
                ' por favor vuelve a intentarlo.');
            }
          });
        });

        $('body').removeClass('hide');
      });
    </script>
  </body>
</html>