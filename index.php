<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pastoral Juvenil de la Diócesis de Tampico - Fuego Nuevo 2019</title>
    <link rel="shortcut icon" href="favicon.png">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/foundation.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <style>.callout { border: none; }</style>
  </head>
  <body class="hide">
    <header>
      <div class="title-bar" data-responsive-toggle="menu" data-hide-for="medium">
        <button class="menu-icon" type="button" data-toggle="menu"></button>
        <div class="title-bar-title">Pastoral Juvenil de la Diócesis de Tampico</div>
      </div>

      <div class="top-bar" id="menu">
        <div class="top-bar-left show-for-medium">
          <ul class="dropdown menu" data-dropdown-menu>
            <li class="menu-text">Pastoral Juvenil de la Diócesis de Tampico</li>
          </ul>
        </div>
        <div class="top-bar-right">
          <ul class="menu">
            <li class="menu-text">Fuego Nuevo 2019</li>
          </ul>
        </div>
      </div>
    </header>
    <main>
      <div class="medium callout">
        <div class="row">
          <div class="large-12 columns">
            <p class="lead text-center">Inicio de sesión al portal de Fuego Nuevo</p>
            <p><strong>¡Bienvenido!</strong> Utiliza el <strong>usuario</strong>
              y <strong>contraseña</strong> que <i>Pastoral Juvenil</i> le hizo llegar
              al coordinador de tu comunidad para entrar al portal de <strong>Fuego Nuevo</strong>, con el fin de
              registrar a tu(s) equipo(s) en las distintas actividades que tendremos este año.
            </p>
            <span id="incorrecto" class="form-error">
              Lo sentimos, el usuario y/o contraseña que ingresaste es incorrecto.
            </span>
          </div>
        </div>

        <form id="login">
          <div class="row">
            <div class="large-2 medium-2 columns">
              <label for="username" class="text-right hide-for-small-only">Usuario</label>
              <label for="username" class="show-for-small-only">Usuario</label>
            </div>

            <div class="large-8 medium-8 columns end">
              <input id="username" name="username" type="text" placeholder="Nombre de usuario">
              <span id="username-vacio" class="form-error">
                Por favor, ingresa tu nombre de usuario.
              </span>
            </div>
          </div>

          <div class="row">
            <div class="large-2 medium-2 columns">
              <label for="password" class="text-right hide-for-small-only">Contraseña</label>
              <label for="password" class="show-for-small-only">Contraseña</label>
            </div>

            <div class="large-8 medium-8 columns end">
              <input id="password" name="password" type="password"
                placeholder="*******">
              <span id="password-vacio" class="form-error">
                Por favor, ingresa tu contraseña.
              </span>
            </div>
          </div>

          <div class="row">
            <div class="large-8 large-offset-2 medium-8 medium-offset-2 columns end">
              <input id="entrar" type="submit" class="medium button expanded" value="ENTRAR">
              <input name="accion" type="hidden" value="log-in">
            </div>
          </div>
        </form>
      </div>
    </main>
    <div id="loading" class="tiny reveal text-center" data-reveal
      data-close-on-click="false" data-close-on-esc="false">
      Espera un momento porfavor...
      <div class="spinner text-center"></div>
    </div>
    <script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script>
      $(document).foundation();

      $(document).ready(function() {
        var usernameInput = $('#username');
        var passwordInput = $('#password');
        var cerrarLoading = function() {
          $('#entrar').attr('disabled', false);
          $('#loading').foundation('close');
        };

        // Trigger de eventos iniciales.
        usernameInput.focus();

        /*
        * Declaracion de EVENTOS.
        */
        $('#login').on('submit', function(evt) {
          // Quitamos clases de error.
          $('.is-invalid-input').removeClass('is-invalid-input');
          $('.form-error').removeClass('is-visible');

          // Validamos que no esten vacios los inputs.
          if ( usernameInput.val().length === 0 ) {
            usernameInput.addClass('is-invalid-input').focus();
            $('#username-vacio').addClass('is-visible');
          } else if ( $('#password').val().length === 0 ) {
            passwordInput.addClass('is-invalid-input').focus();
            $('#password-vacio').addClass('is-visible');
          } else {
            // Deshabilitamos el boton de enviar por si las moscas.
            $('#entrar').attr('disabled', true);

            // Todo correcto, mandamos datos.
            var data = $(this).serialize();

            // Abrimos loading.
            $('#loading').foundation('open');

            $.post('php/api.php', data, function(response) {
              if (response.status === 'OK') {
                window.location.href = 'views/';
              } else {
                $('#incorrecto').addClass('is-visible');
                usernameInput.focus();

                cerrarLoading();
              }
            }, 'json')
            .fail(function() {
              alert('Falló la conexión al servidor, por favor vuelve a intentarlo.');
              cerrarLoading();
            });
          }

          return evt.preventDefault();
        });
        /*  */

        $('body').removeClass('hide')
                 .find('#username')
                 .focus();
      });
    </script>
  </body>
</html>
