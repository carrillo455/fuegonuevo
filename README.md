#Getting Started

Install [Gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)

& Sass within `npm install gulp-sass`

If you modify .sass, run `gulp styles` to update the main css.

To init browser-sync, run:
`browser-sync start --proxy localhost/fuegonuevo --serveStatic '*.php, css/*.css, views/*.php' --files '*.php, css/*.css, views/*.php'`