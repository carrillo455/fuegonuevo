<?php
  date_default_timezone_set('America/Mexico_City');
  // Validar si ya se hizo 'session_start' en algun otro archivo y evitar el error.
  if (!session_id()) {
    session_start();
  }

  // Mandar llamar el archivo de conexion.
  require_once 'conexion.php';

  // Recibir la accion desde el lado cliente.
  $accion = $_POST['accion'];

  switch ($accion) {
    case 'log-in':
      $username = $_POST['username'];
      $password = $_POST['password'];

      $query = $pdo->prepare("SELECT u.usuarios_id AS id, u.username,
          un.usuarios_niveles_id AS id_nivel, un.nombre AS nombre_nivel,
          GROUP_CONCAT(uc.comunidades_id ORDER BY uc.comunidades_id SEPARATOR ',' ) AS comunidades
          FROM usuarios u
          INNER JOIN usuarios_niveles un
            ON un.usuarios_niveles_id = u.usuarios_niveles_id
          LEFT JOIN usuarios_comunidades uc
            ON uc.usuarios_id = u.usuarios_id
          WHERE CAST(username AS BINARY) = :username
            AND CAST(password AS BINARY) = :password
            AND u.borrado = 0
            AND un.borrado = 0
          GROUP BY u.usuarios_id, u.username, un.usuarios_niveles_id, un.nombre");
      $query->execute(array('username' => $username, 'password' => $password));
      $usuario = $query->fetch(PDO::FETCH_ASSOC);

      if ($usuario) {
        $_SESSION['usuario']['id'] = $usuario['id'];
        $_SESSION['usuario']['username'] = $usuario['username'];
        $_SESSION['usuario']['nivel'] = array();
        $_SESSION['usuario']['nivel']['id'] = $usuario['id_nivel'];
        $_SESSION['usuario']['nivel']['nombre'] = $usuario['nombre_nivel'];
        $_SESSION['usuario']['comunidades'] = explode(',', $usuario['comunidades']);

        $query = $pdo->query("SELECT clave
          FROM usuarios_accesos ua
          INNER JOIN usuarios_permisos up
            ON up.usuarios_accesos_id = ua.usuarios_accesos_id
          WHERE up.borrado = 0
            AND up.usuarios_niveles_id = " . $usuario['id_nivel']);

        $claves = $query->fetchAll(PDO::FETCH_COLUMN);
        $_SESSION['usuario']['claves'] = $claves;

        echo json_encode(array('status' => 'OK'));
      } else {
        echo json_encode(array('status' => 'WRONG'));
      }
    break;

    case 'log-out':
      session_destroy();
      session_unset();

      echo json_encode(array('status' => 'OK'));
    break;

    case 'check':
      $status = isset($_SESSION['usuario']) ? 'OK' : 'BYE';
      echo json_encode(array('status' => $status));
    break;

    case 'borrar-comunidad':
      $comunidades_id = $_POST['id'];

      $update = $pdo->prepare("UPDATE comunidades
        SET borrado = 1
        WHERE comunidades_id = :comunidades_id");
      $ok = $update->execute(array('comunidades_id' => $comunidades_id));
      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR BORRAR COMUNIDAD',
            'msg' => '¡Algo sucedió! Por favor, vuelve a intentarlo de nuevo.'
          )
        );
        exit;
      }

      echo json_encode(array(
          'status' => 'OK',
          'msg' => 'La comunidad fue <strong>borrada</strong> correctamente.' .
            '<br><br>Recuerda que en cualquier momento puedes <strong>' .
            'incorporarla</strong> nuevamente dando clic en el botón de ' .
            '<strong>Incorporar</strong>'
        )
      );
    break;

    case 'bloquear-usuario':
      $usuarios_id = $_POST['id'];

      $update = $pdo->prepare("UPDATE usuarios
        SET borrado = 1
        WHERE usuarios_id = :usuarios_id");
      $ok = $update->execute(array('usuarios_id' => $usuarios_id));
      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR BLOQUEAR USUARIO',
            'msg' => '¡Algo sucedió! Por favor, vuelve a intentarlo de nuevo.'
          )
        );
        exit;
      }

      echo json_encode(array(
          'status' => 'OK',
          'msg' => 'El usuario fue <strong>bloqueado</strong> correctamente.'
        )
      );
    break;

    case 'incorporar-comunidad':
      $comunidades_id = $_POST['id'];

      $update = $pdo->prepare("UPDATE comunidades
        SET borrado = 0
        WHERE comunidades_id = :comunidades_id");
      $ok = $update->execute(array('comunidades_id' => $comunidades_id));
      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR INCORPORAR COMUNIDAD',
            'msg' => '¡Algo sucedió! Por favor, vuelve a intentarlo de nuevo.'
          )
        );
        exit;
      }

      echo json_encode(array(
          'status' => 'OK',
          'msg' => 'La comunidad fue <strong>incorporada</strong> correctamente.'
        )
      );
    break;

    case 'desbloquear-usuario':
      $usuarios_id = $_POST['id'];

      $update = $pdo->prepare("UPDATE usuarios
        SET borrado = 0
        WHERE usuarios_id = :usuarios_id");
      $ok = $update->execute(array('usuarios_id' => $usuarios_id));
      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR DESBLOQUEAR USUARIO',
            'msg' => '¡Algo sucedió! Por favor, vuelve a intentarlo de nuevo.'
          )
        );
        exit;
      }

      echo json_encode(array(
          'status' => 'OK',
          'msg' => 'El usuario fue <strong>desbloqueado</strong> correctamente.'
        )
      );
    break;

    case 'obtener-comunidades':
      $modulo = isset($_POST['modulo']) ? $_POST['modulo'] : false;
      $usuarios_id = $_SESSION['usuario']['id'];

      if (in_array('SUD0', $_SESSION['usuario']['claves'])) {
        switch ($modulo) {
          case 'users': // comunidades que no tienen usuario.
            $query = $pdo->query("SELECT comunidades_id AS id, nombre
              FROM comunidades
              WHERE comunidades_id NOT IN (SELECT comunidades_id FROM usuarios_comunidades)
                AND borrado = 0");
          break;

          case 'export': // todas las comunidades existentes.
          default:
            $query = $pdo->query("SELECT comunidades_id AS id, nombre
              FROM comunidades
              WHERE borrado = 0");
          break;

          // default: break;
        }
      } else {
        $query = $pdo->query("SELECT c.comunidades_id AS id, c.nombre
          FROM comunidades c
          INNER JOIN usuarios_comunidades uc ON uc.comunidades_id = c.comunidades_id
          WHERE usuarios_id = $usuarios_id
            AND c.borrado = 0
            AND uc.borrado = 0");
      }

      if (!$query) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER COMUNIDAD',
            'msg' => '¡Lo sentimos!, no fue posible obtener información esencial' .
              ' para la inscripción. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      $data = $query->fetchAll(PDO::FETCH_ASSOC);
      echo json_encode(array(
          'status' => 'OK',
          'data' => $data
        )
      );
    break;

    case 'obtener-disciplinas':
      $query = $pdo->query("SELECT disciplinas_id AS id,
        CONCAT(nombre, ' ', rama) AS nombre,
        min_integrantes AS min, max_integrantes AS max
        FROM disciplinas
        WHERE borrado = 0");

      if (!$query) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER DISCIPLINAS',
            'msg' => '¡Lo sentimos!, no fue posible obtener información esencial' .
              ' para la inscripción. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      $data = $query->fetchAll(PDO::FETCH_ASSOC);
      echo json_encode(array(
          'status' => 'OK',
          'data' => $data
        )
      );
    break;

    case 'obtener-mas-informacion':
      $inscripciones_id = $_POST['inscripcion'];

      $query = $pdo->prepare("SELECT c.nombre AS comunidad,
        TRIM(CONCAT(co.nombre, ' ', co.apellido_paterno,' ', co.apellido_materno)) AS coordinadorNombre,
        co.celular AS coordinadorCelular, co.email AS coordinadorEmail,
        DATE_FORMAT(co.fecha_nacimiento, '%d/%m/%Y') AS coordinadorFechaNacimiento,
        TRIM(CONCAT(de.nombre, ' ', de.apellido_paterno,' ', de.apellido_materno)) AS delegadoNombre,
        de.celular AS delegadoCelular, de.email AS delegadoEmail,
        DATE_FORMAT(de.fecha_nacimiento, '%d/%m/%Y') AS delegadoFechaNacimiento,
        url_carta_sacerdote AS carta, url_lista_miembros AS lista,
        CONCAT(d.nombre, ' ', d.rama) AS disciplina, si.nombre AS status
        FROM inscripciones i
        INNER JOIN informacion_base ib ON ib.informacion_base_id = i.informacion_base_id
        INNER JOIN comunidades c ON c.comunidades_id = ib.comunidades_id
        INNER JOIN coordinadores co ON co.coordinadores_id = ib.coordinadores_id
        INNER JOIN delegados de ON de.delegados_id = ib.delegados_id
        INNER JOIN disciplinas d ON d.disciplinas_id = i.disciplinas_id
        INNER JOIN status_inscripciones si ON si.status_id = i.status_id
        WHERE i.inscripciones_id = :inscripciones_id
          AND i.borrado = 0");

      $ok = $query->execute(array(
          'inscripciones_id' => $inscripciones_id
        )
      );

      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER INFO-1',
            'msg' => '¡Algo sucedió! Por favor, vuelve a intentarlo de nuevo.'
          )
        );
        exit;
      }

      $informacion_base = $query->fetch(PDO::FETCH_ASSOC);

      $query = $pdo->prepare("SELECT ti.nombre AS tipoIntegrante,
        TRIM(CONCAT(i.nombre, ' ', i.apellido_paterno, ' ', i.apellido_materno)) AS nombre,
        DATE_FORMAT(i.fecha_nacimiento, '%d/%m/%Y') AS fechaNacimiento,
        url_fotografia AS fotografia, url_credencial AS credencial
        FROM integrantes i
        INNER JOIN tipos_integrantes ti ON ti.tipos_integrantes_id = i.tipos_integrantes_id
        WHERE i.inscripciones_id = :inscripciones_id
          AND i.borrado = 0");

      $ok = $query->execute(array(
          'inscripciones_id' => $inscripciones_id
        )
      );

      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER INFO-2',
            'msg' => '¡Algo sucedió! Por favor, vuelve a intentarlo de nuevo.'
          )
        );
        exit;
      }

      $integrantes = $query->fetchAll(PDO::FETCH_ASSOC);

      echo json_encode(array(
          'status' => 'OK',
          'data' => array(
            'informacionBase' => $informacion_base,
            'integrantes' => $integrantes
          )
        )
      );
    break;

    case 'obtener-informacion-base':
      $comunidades_id = $_POST['comunidad'];

      $query = $pdo->prepare("SELECT informacion_base_id AS id,
        coordinadores_id, delegados_id,
        url_carta_sacerdote AS carta,
        url_lista_miembros AS lista
        FROM informacion_base
        WHERE comunidades_id = :comunidades_id");
      $ok = $query->execute(array('comunidades_id' => $comunidades_id));
      if (!$ok) {
        echo json_encode(array(
            'status' => 'OK',
            'msg' => '¡Oops algo sucedió! Por favor, vuelve a iniciar sesión en el sistema.'
          )
        );
        exit;
      }

      $informacion_base = $query->fetch(PDO::FETCH_ASSOC);
      if (!$informacion_base) {
        echo json_encode(array(
          'status' => 'OK',
          'data' => false
          )
        );
        exit;
      }

      // COORDINADOR
      $query = $pdo->prepare("SELECT coordinadores_id AS id,
        nombre, apellido_paterno AS apellidoPaterno,
        apellido_materno AS apellidoMaterno, celular, email,
        DATE_FORMAT(fecha_nacimiento, '%d/%m/%Y') AS fechaNacimiento
        FROM coordinadores
        WHERE coordinadores_id = :coordinadores_id");
      $ok = $query->execute(array(
          'coordinadores_id' => $informacion_base['coordinadores_id']
        )
      );
      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER COORDINADOR',
            'msg' => '¡Lo sentimos!, no fue posible obtener información esencial' .
              ' para la inscripción. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      $coordinador = $query->fetch(PDO::FETCH_ASSOC);

      // DELEGADO
      $query = $pdo->prepare("SELECT delegados_id AS id,
        nombre, apellido_paterno AS apellidoPaterno,
        apellido_materno AS apellidoMaterno, celular, email,
        DATE_FORMAT(fecha_nacimiento, '%d/%m/%Y') AS fechaNacimiento
        FROM delegados
        WHERE delegados_id = :delegados_id");
      $ok = $query->execute(array(
          'delegados_id' => $informacion_base['delegados_id']
        )
      );
      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER DELEGADO',
            'msg' => '¡Lo sentimos!, no fue posible obtener información esencial' .
              ' para la inscripción. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      $delegado = $query->fetch(PDO::FETCH_ASSOC);

      echo json_encode(array(
          'status' => 'OK',
          'data' => array(
            'informacionBase' => $informacion_base,
            'coordinador' => $coordinador,
            'delegado' => $delegado
          )
        )
      );
    break;

    case 'obtener-inscripcion':
      $informacion_base_id = $_POST['informacion-base'];
      $disciplinas_id = $_POST['disciplina'];

      $query = $pdo->prepare("SELECT inscripciones_id
        FROM inscripciones
        WHERE informacion_base_id = :informacion_base_id
          AND disciplinas_id = :disciplinas_id
          AND borrado = 0");
      $ok = $query->execute(array(
          'informacion_base_id' => $informacion_base_id,
          'disciplinas_id' => $disciplinas_id
        )
      );
      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER INSCRIPCION',
            'msg' => '¡Lo sentimos!, no fue posible obtener información esencial' .
              ' para la inscripción. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      $inscripciones_id = $query->fetchColumn();

      echo json_encode(array(
          'status' => 'OK',
          'data' => $inscripciones_id
        )
      );
    break;

    case 'obtener-integrantes':
      $inscripciones_id = $_POST['inscripcion'];

      $query = $pdo->prepare("SELECT integrantes_id AS id,
        tipos_integrantes_id AS tipo,
        nombre, apellido_paterno AS apellidoPaterno,
        apellido_materno AS apellidoMaterno,
        DATE_FORMAT(fecha_nacimiento, '%d/%m/%Y') AS fechaNacimiento,
        url_fotografia AS fotografia, url_credencial AS credencial
        FROM integrantes
        WHERE inscripciones_id = :inscripciones_id
          AND borrado = 0");
      $ok = $query->execute(array('inscripciones_id' => $inscripciones_id));

      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER INTEGRANTES',
            'msg' => '¡Lo sentimos!, no fue posible obtener información esencial' .
              ' para la inscripción. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      $integrantes = $query->fetchAll(PDO::FETCH_ASSOC);
      echo json_encode(array(
          'status' => 'OK',
          'data' => $integrantes
        )
      );
    break;

    case 'obtener-status-inscripciones':
      $query = $pdo->query("SELECT status_id AS id, nombre
        FROM status_inscripciones
        ORDER BY id ASC");

      if (!$query) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER STATUS INSCRIPCIONES',
            'msg' => '¡Lo sentimos!, no fue posible obtener información esencial' .
              ' para la inscripción. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      $data = $query->fetchAll(PDO::FETCH_ASSOC);
      echo json_encode(array(
          'status' => 'OK',
          'data' => $data
        )
      );
    break;

    case 'obtener-tipos-comunidades':
      $query = $pdo->query("SELECT tipos_comunidades_id AS id, nombre
        FROM tipos_comunidades
        WHERE borrado = 0");

      if (!$query) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER TIPOS COMUNIDADES',
            'msg' => '¡Lo sentimos!, no fue posible obtener información esencial' .
              ' .Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      $data = $query->fetchAll(PDO::FETCH_ASSOC);
      echo json_encode(array(
          'status' => 'OK',
          'data' => $data
        )
      );
    break;

    case 'obtener-tipos-integrantes':
      $query = $pdo->query("SELECT tipos_integrantes_id AS id, nombre,
        min_integrantes AS min, max_integrantes AS max
        FROM tipos_integrantes
        WHERE borrado = 0");

      if (!$query) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER TIPOS INTEGRANTES',
            'msg' => '¡Lo sentimos!, no fue posible obtener información esencial' .
              ' para la inscripción. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      $data = $query->fetchAll(PDO::FETCH_ASSOC);
      echo json_encode(array(
          'status' => 'OK',
          'data' => $data
        )
      );
    break;

    case 'obtener-tipos-templos':
      $query = $pdo->query("SELECT tipos_templos_id AS id, nombre
        FROM tipos_templos
        WHERE borrado = 0");

      if (!$query) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER TIPOS TEMPLOS',
            'msg' => '¡Lo sentimos!, no fue posible obtener información esencial' .
              ' para la inscripción. Por favor, vuelve a cargar la página.'
          )
        );
        exit;
      }

      $data = $query->fetchAll(PDO::FETCH_ASSOC);
      echo json_encode(array(
          'status' => 'OK',
          'data' => $data
        )
      );
    break;

    case 'cambiar-status-inscripcion':
      $inscripciones_id = $_POST['inscripcion'];
      $status_id = $_POST['status'];
      $usuarios_id = $_SESSION['usuario']['id'];

      $pdo->beginTransaction();

      $update = $pdo->prepare("UPDATE inscripciones
        SET status_id = :status_id
        WHERE inscripciones_id = :inscripciones_id");
      $ok = $update->execute(array(
          'status_id' => $status_id,
          'inscripciones_id' => $inscripciones_id
        )
      );

      if (!$ok) {
        $pdo->rollback();
        echo json_encode(array(
            'status' => 'ERROR CAMBIAR STATUS',
            'msg' => '¡Algo sucedió! Por favor, vuelve a intentarlo de nuevo.'
          )
        );
        exit;
      }

      $insert = $pdo->prepare("INSERT INTO log_inscripciones
        SELECT NULL, :sentencia, inscripciones_id, disciplinas_id,
          status_id, borrado, :usuarios_id, CURRENT_TIMESTAMP
        FROM inscripciones
        WHERE inscripciones_id = :inscripciones_id");
      $ok = $insert->execute(array(
          'sentencia' => 'UPDATE STATUS',
          'usuarios_id' => $usuarios_id,
          'inscripciones_id' => $inscripciones_id
        )
      );

      if (!$ok) {
        $pdo->rollback();
        echo json_encode(array(
            'status' => 'ERROR INSERTAR LOG STATUS',
            'msg' => '¡Algo sucedió! Por favor, vuelve a intentarlo de nuevo.'
          )
        );
        exit;
      }

      $pdo->commit();
      echo json_encode(array(
          'status' => 'OK',
          'msg' => 'El status fue cambiado correctamente.'
        )
      );
    break;

    case 'guardar-comunidad':
      $nombre_templo = $_POST['nombre-templo'];
      $tipos_templos_id = $_POST['tipo-templo'];
      $nombre_comunidad = $_POST['nombre-comunidad'];
      $tipos_comunidades_id = $_POST['tipo-comunidad'];

      $pdo->beginTransaction();

      $insert = $pdo->prepare("INSERT INTO templos
        SET nombre = :nombre_templo,
          tipos_templos_id = :tipos_templos_id,
          templos_id_fk = NULL");
      $ok = $insert->execute(array(
          'nombre_templo' => $nombre_templo,
          'tipos_templos_id' => $tipos_templos_id
        )
      );

      if (!$ok) {
        $pdo->rollback();
        echo json_encode(array(
            'status' => 'ERROR INSERT TEMPLO',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' a generar el usuario.'
          )
        );
        exit;
      }

      $templos_id = $pdo->lastInsertId();

      $insert = $pdo->prepare("INSERT INTO comunidades
        SET nombre = :nombre_comunidad,
          tipos_comunidades_id = :tipos_comunidades_id,
          templos_id = :templos_id");
      $ok = $insert->execute(array(
          'nombre_comunidad' => $nombre_comunidad,
          'tipos_comunidades_id' => $tipos_comunidades_id,
          'templos_id' => $templos_id
        )
      );

      if (!$ok) {
        $pdo->rollback();
        echo json_encode(array(
            'status' => 'ERROR INSERT COMUNIDAD',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' a generar el usuario.'
          )
        );
        exit;
      }

      $pdo->commit();
      echo json_encode(array(
          'status' => 'OK',
          'msg' => 'La comunidad <strong>' . $nombre_comunidad .
            '</strong> fue ingresada correctamente.'
        )
      );
    break;

    case 'guardar-informacion-base':
      function format_date($dstring) {
        $earray = explode('/', $dstring);
        if (count($earray) < 3) {
          return null;
        }
        $rarray = array_reverse($earray);
        return implode('-', $rarray);
      }

      $is_update = is_numeric($_POST['informacion-base']);
      $sentencia = $is_update ? 'UPDATE' : 'INSERT INTO';
      $informacion_base_id = $is_update ? $_POST['informacion-base'] : false;
      $comunidades_id = $_POST['comunidad'];
      $coordinador = $_POST['coordinador'];
      $delegado = $_POST['delegado'];
      $carta_sacerdote = $_FILES['archivo-carta'];
      $url_carta_sacerdote = isset($_POST['url-carta']) ? $_POST['url-carta'] : false;
      $lista_miembros = $_FILES['archivo-lista'];
      $url_lista_miembros = isset($_POST['url-lista']) ? $_POST['url-lista'] : false;

      $uniqid = uniqid();
      $root = '../';
      $paths = array(
        'carta_sacerdote' => 'files/' . $uniqid . '/',
        'lista_miembros' => 'files/' . $uniqid . '/',
      );
      $types = array(
        'carta_sacerdote' => array(
          'application/pdf', 'image/jpeg', 'image/jpg'
        ),
        'lista_miembros' => array(
          'application/pdf'
        )
      );
      $where = array(
        'informacion_base' => '',
        'coordinador' => '',
        'delegado' => ''
      );
      $values = array(
        'informacion_base' => array(
          'comunidades_id' => $comunidades_id,
          'coordinadores_id' => 0,
          'delegados_id' => 0,
          'uniqid' => $uniqid,
          'url_carta_sacerdote' => 'null',
          'path_carta_sacerdote' => 'null',
          'url_lista_miembros' => 'null',
          'path_lista_miembros' => 'null'
        ),
        'coordinador' => array(
          'nombre' => $coordinador['nombre'],
          'apellido_paterno' => $coordinador['apellido-paterno'],
          'apellido_materno' => $coordinador['apellido-materno'],
          'celular' => $coordinador['celular'],
          'email' => $coordinador['email'],
          'fecha_nacimiento' => format_date($coordinador['fecha-nacimiento'])
        ),
        'delegado' => array(
          'nombre' => $delegado['nombre'],
          'apellido_paterno' => $delegado['apellido-paterno'],
          'apellido_materno' => $delegado['apellido-materno'],
          'celular' => $delegado['celular'],
          'email' => $delegado['email'],
          'fecha_nacimiento' => format_date($delegado['fecha-nacimiento'])
        )
      );
      $success = array(
        'status' => 'OK',
        'msg' => 'Tu información base fue guardada correctamente.' .
          ' Tienes hasta <strong>21/04/2019</strong> para completar el registro de todos' .
          ' tus integrantes.'
      );
      $errors = array(
        'informacion_base' => array(
          'sql' => array(
            'status' => 'ERROR ' . $sentencia . ' INFORMACION BASE',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' guardar la información.'
          ),
          'uniqid' => array(
            'status' => 'ERROR OBTENER UNIQID',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' guardar la información.'
          ),
          'log' => array(
            'status' => 'ERROR LOG INFORMACION BASE',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' guardar la información.'
          )
        ),
        'coordinador' => array(
          'status' => 'ERROR ' . $sentencia . ' COORDINADOR',
          'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
            ' guardar la información.'
        ),
        'delegado' => array(
          'status' => 'ERROR ' . $sentencia . ' DELEGADO',
          'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
            ' guardar la información.'
        ),
        'carta_sacerdote' => array(
          'type' => array(
            'status' => 'ERROR TYPE CARTA SACERDOTE',
            'msg' => '¡Lo sentimos!, no fue posible guardar la información.<br>' .
              'Por favor, revisa que la CARTA DEL SACERDOTE este en PDF o JPG.'
          ),
          'save' => array(
            'status' => 'ERROR SAVE CARTA SACERDOTE',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
            ' guardar la información.'
          )
        ),
        'lista_miembros' => array(
          'type' => array(
            'status' => 'ERROR TYPE LISTA MIEMBROS',
            'msg' => '¡Lo sentimos!, no fue posible guardar la información.<br>' .
              'Por favor, revisa que la CARTA DEL SACERDOTE este en PDF o JPG.'
          ),
          'save' => array(
            'status' => 'ERROR SAVE LISTA MIEMBROS',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
            ' guardar la información.'
          )
        )
      );
      $usuarios_id = $_SESSION['usuario']['id'];

      // Agregamos unos valores previos en caso de ser UPDATE.
      if ($is_update) {
        $where['informacion_base'] = 'WHERE informacion_base_id = :informacion_base_id';
        $where['coordinador'] = 'WHERE coordinadores_id = :coordinadores_id';
        $where['delegado'] = 'WHERE delegados_id = :delegados_id';

        $values['informacion_base']['informacion_base_id'] = $informacion_base_id;
        $values['coordinador']['coordinadores_id'] = $coordinador['id'];
        $values['delegado']['delegados_id'] = $delegado['id'];

        $query = $pdo->prepare("SELECT uniqid
          FROM informacion_base
          WHERE informacion_base_id = :informacion_base_id");
        $ok = $query->execute(array('informacion_base_id' => $informacion_base_id));
        if (!$ok) {
          echo json_encode($errors['informacion_base']['uniqid']);
          exit;
        }

        $uniqid = $query->fetchColumn();
        $paths = array(
          'carta_sacerdote' => 'files/' . $uniqid . '/',
          'lista_miembros' => 'files/' . $uniqid . '/'
        );

        $values['informacion_base']['uniqid'] = $uniqid;
      }

      // Iniciamos la transaccion.
      $pdo->beginTransaction();

      /*
      * COORDINADOR
      */
      $sql = $pdo->prepare($sentencia . " coordinadores
        SET nombre = :nombre,
          apellido_paterno = :apellido_paterno,
          apellido_materno = :apellido_materno,
          celular = :celular,
          email = :email,
          fecha_nacimiento = :fecha_nacimiento " .
        $where['coordinador']);

      $ok = $sql->execute($values['coordinador']);
      if (!$ok) {
        $pdo->rollback();
        echo json_encode($errors['coordinador']);
        exit;
      }

      $coordinador['id'] = !empty($coordinador['id']) ? $coordinador['id'] : $pdo->lastInsertId();
      $values['informacion_base']['coordinadores_id'] = $coordinador['id'];
      /* FIN COORDINADOR */

      /*
      * DELEGADO
      */
      $sql = $pdo->prepare($sentencia . " delegados
        SET nombre = :nombre,
          apellido_paterno = :apellido_paterno,
          apellido_materno = :apellido_materno,
          celular = :celular,
          email = :email,
          fecha_nacimiento = NULLIF(:fecha_nacimiento, 'null') " .
        $where['delegado']);

      $ok = $sql->execute($values['delegado']);
      if (!$ok) {
        $pdo->rollback();
        echo json_encode($errors['delegado']);
        exit;
      }

      $delegado['id'] = !empty($delegado['id']) ? $delegado['id'] : $pdo->lastInsertId();
      $values['informacion_base']['delegados_id'] = $delegado['id'];
      /* FIN DELEGADO */

      /*
      * CARTA DEL SACERDOTE
      */
      if (empty($carta_sacerdote['name']) && $url_carta_sacerdote) {
        $values['informacion_base']['url_carta_sacerdote'] = $url_carta_sacerdote;
        $values['informacion_base']['path_carta_sacerdote'] = str_replace($host, '', $url_carta_sacerdote);
      } else if ($carta_sacerdote['error'] === 0) {
        $folder = $root . $paths['carta_sacerdote'];
        if (!file_exists($folder)) {
          mkdir($folder, 0777, true);
        }

        $filename = $carta_sacerdote['name'];
        $filename_exploded = explode('.', $filename);
        $extension = end($filename_exploded);
        $paths['carta_sacerdote'] .= 'carta-sacerdote.' . $extension;

        if (!in_array($carta_sacerdote['type'], $types['carta_sacerdote'])) {
          $pdf->rollback();
          echo json_encode($errors['carta_sacerdote']['type']);
          exit;
        }

        $target = $root . $paths['carta_sacerdote'];
        $saved = move_uploaded_file($carta_sacerdote['tmp_name'], $target);
        if (!$saved) {
          $pdf->rollback();
          echo json_encode($errors['carta_sacerdote']['save']);
          exit;
        }

        $values['informacion_base']['url_carta_sacerdote'] = $host . $paths['carta_sacerdote'];
        $values['informacion_base']['path_carta_sacerdote'] = $paths['carta_sacerdote'];
      }
      /* FIN CARTA DEL SACERDOTE */

      /*
      * LISTA DE MIEMBROS
      */
      if (empty($lista_miembros['name']) && $url_lista_miembros) {
        $values['informacion_base']['url_lista_miembros'] = $url_lista_miembros;
        $values['informacion_base']['path_lista_miembros'] = str_replace($host, '', $url_lista_miembros);
      } else if ($lista_miembros['error'] === 0) {
        $folder = $root . $paths['lista_miembros'];
        if (!file_exists($folder)) {
          mkdir($folder, 0777, true);
        }

        $filename = $lista_miembros['name'];
        $filename_exploded = explode('.', $filename);
        $extension = end($filename_exploded);
        $paths['lista_miembros'] .= 'lista-miembros.' . $extension;

        if (!in_array($lista_miembros['type'], $types['lista_miembros'])) {
          $pdf->rollback();
          echo json_encode($errors['lista_miembros']['type']);
          exit;
        }

        $target = '../' . $paths['lista_miembros'];
        $saved = move_uploaded_file($lista_miembros['tmp_name'], $target);
        if (!$saved) {
          $pdf->rollback();
          echo json_encode($errors['lista_miembros']['save']);
          exit;
        }

        $values['informacion_base']['url_lista_miembros'] = $host . $paths['lista_miembros'];
        $values['informacion_base']['path_lista_miembros'] = $paths['lista_miembros'];
      }
      /* FIN LISTA MIEMBROS */

      /*
      * INFORMACION BASE
      */
      $sql = $pdo->prepare($sentencia . " informacion_base
        SET comunidades_id = :comunidades_id,
          coordinadores_id = :coordinadores_id,
          delegados_id = :delegados_id,
          uniqid = :uniqid,
          url_carta_sacerdote = NULLIF(:url_carta_sacerdote, 'null'),
          path_carta_sacerdote = NULLIF(:path_carta_sacerdote, 'null'),
          url_lista_miembros = NULLIF(:url_lista_miembros, 'null'),
          path_lista_miembros = NULLIF(:path_lista_miembros, 'null') " .
        $where['informacion_base']);

      $ok = $sql->execute($values['informacion_base']);
      if (!$ok) {
        $pdo->rollback();
        echo json_encode($errors['informacion_base']['sql']);
        exit;
      }

      $informacion_base_id = !$informacion_base_id ? $pdo->lastInsertId() : $informacion_base_id;

      if ($sql->rowCount() > 0) {
        $values['log']['informacion_base']['sentencia'] = $sentencia;
        $values['log']['informacion_base']['usuarios_id'] = $usuarios_id;
        $values['log']['informacion_base']['informacion_base_id'] = $informacion_base_id;

        $insert = $pdo->prepare("INSERT INTO log_informacion_base
          SELECT NULL, :sentencia, informacion_base_id, comunidades_id,
            coordinadores_id, delegados_id, uniqid,
            url_carta_sacerdote, path_carta_sacerdote,
            url_lista_miembros, path_lista_miembros,
            borrado, :usuarios_id, CURRENT_TIMESTAMP
          FROM informacion_base
          WHERE informacion_base_id = :informacion_base_id");
        $ok = $insert->execute($values['log']['informacion_base']);
        if (!$ok) {
          $pdo->rollback();
          echo json_encode($errors['informacion_base']['log']);
          exit;
        }
      }
      /* FIN INFORMACION BASE */

      $pdo->commit();
      $success['data'] = array(
        'informacion-base' => $informacion_base_id,
        'coordinador[id]' => $coordinador['id'],
        'delegado[id]' => $delegado['id']
      );
      echo json_encode($success);
    break;

    case 'guardar-inscripcion':
      // var_dump($_POST);
      // var_dump($_FILES);
      // exit;

      function format_date($dstring) {
        $earray = explode('/', $dstring);
        if (count($earray) < 3) {
          return null;
        }
        $rarray = array_reverse($earray);
        return implode('-', $rarray);
      }

      $comunidades_id = $_POST['comunidad'];
      $disciplinas_id = $_POST['disciplina'];
      $query = $pdo->prepare("SELECT informacion_base_id AS id, uniqid
        FROM informacion_base
        WHERE comunidades_id = :comunidades_id");
      $ok = $query->execute(array('comunidades_id' => $comunidades_id));
      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER INFORMACION BASE',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' guardar la información.'
          )
        );
        exit;
      }
      $informacion_base = $query->fetch(PDO::FETCH_ASSOC);
      if (!$informacion_base) {
        echo json_encode(array(
            'status' => 'LACK',
            'msg' => 'No fue posible guardar la inscripción.' .
              ' Antes de inscribir a tu equipo es necesario que envíes tu <strong>Información ' .
              ' Base</strong> acerca de tu comunidad.'
          )
        );
        exit;
      }

      $informacion_base_id = $informacion_base['id'];
      $is_update = is_numeric($_POST['inscripcion']);
      $sentencia = $is_update ? 'UPDATE' : 'INSERT INTO';
      $inscripciones_id = $is_update ? $_POST['inscripcion'] : false;
      $integrantes = $_POST['integrantes'];
      $root = '../';
      $uniqid = $informacion_base['uniqid'];
      $paths = array(
        'integrante' => 'files/' . $uniqid . '/' .
          str_pad($disciplinas_id, 2, '0', STR_PAD_LEFT) . '/'
      );
      $types = array(
        'integrante' => array(
          "jpg" => "image/jpg",
          "jpeg" => "image/jpeg",
          "png" => "image/png"
        )
      );
      $where = array(
        'inscripcion' => '',
        'integrante' => ''
      );
      $values = array(
        'inscripcion' => array(
          'informacion_base_id' => $informacion_base_id,
          'disciplinas_id' => $disciplinas_id
        ),
        'integrante' => array(),
        'log' => array(
          'inscripcion' => array()
        )
      );
      $success = array(
        'status' => 'OK',
        'msg' => 'La inscripción fue guardada correctamente.' .
          ' Tienes hasta <strong>21/04/2019</strong> para completar el registro de todos' .
          ' tus integrantes.'
      );
      $errors = array(
        'inscripcion' => array(
          'status' => 'ERROR INSERT INTO INSCRIPCION',
          'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
            ' guardar la información.'
        ),
        'integrante' => array(
          'sql' => array(
            'status' => 'ERROR ' . $sentencia . ' INTEGRANTE',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' guardar la información.'
          ),
          'delete' => array(
            'status' => 'ERROR DELETE INTEGRANTES',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' guardar la información.'
          ),
          'save' => array(
            'status' => 'ERROR SAVE FOTOGRAFIA O CREDENCIAL',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' guardar la información.'
          )
        )
      );
      $usuarios_id = $_SESSION['usuario']['id'];

      // Agregamos unos valores previos en caso de ser UPDATE.
      if ($is_update) {
        $where['inscripcion'] = 'WHERE inscripciones_id = :inscripciones_id';
        $values['inscripcion']['inscripciones_id'] = $inscripciones_id;
      }

      // Iniciamos la transaccion.
      $pdo->beginTransaction();

      /*
      * INSCRIPCION
      */
      $sql = $pdo->prepare($sentencia . " inscripciones
        SET informacion_base_id = :informacion_base_id,
          disciplinas_id = :disciplinas_id " .
        $where['inscripcion']);

      $ok = $sql->execute($values['inscripcion']);
      if (!$ok) {
        $pdo->rollback();
        echo json_encode($errors['inscripcion']);
        exit;
      }

      $inscripciones_id = $inscripciones_id ? $inscripciones_id : $pdo->lastInsertId();

      $insert = $pdo->prepare("INSERT INTO log_inscripciones
        SELECT NULL, :sentencia, inscripciones_id, disciplinas_id,
          status_id, borrado, :usuarios_id, CURRENT_TIMESTAMP
        FROM inscripciones
        WHERE inscripciones_id = :inscripciones_id");
      $ok = $insert->execute(array(
          'sentencia' => $sentencia,
          'usuarios_id' => $usuarios_id,
          'inscripciones_id' => $inscripciones_id
        )
      );

      if (!$ok) {
        $pdo->rollback();
        echo json_encode(array(
            'status' => 'ERROR INSERTAR LOG INSCRIPCIONES',
            'msg' => '¡Algo sucedió! Por favor, vuelve a intentarlo de nuevo.'
          )
        );
        exit;
      }
      /* FIN INSCRIPCION */

      /*
      * INTEGRANTES
      */
      // Borrar integrantes en caso de ser update.
      if ($is_update) {
        $update = $pdo->prepare("UPDATE integrantes
          SET borrado = 1
          WHERE inscripciones_id = :inscripciones_id");
        $ok = $update->execute(array('inscripciones_id' => $inscripciones_id));
        if (!$ok) {
          $pdo->rollback();
          echo json_encode($errors['integrante']['delete']);
          exit;
        }
      }

      $folder = $root . $paths['integrante'];
      if (!file_exists($folder)) {
        mkdir($folder, 0777, true);
      }

      foreach ($integrantes['id'] as $i => $id) {
        // $values['integrante']['comunidades_id'] = $comunidades_id;
        // $values['integrante']['disciplinas_id'] = $disciplinas_id;
        $is_update = !empty($id) ? true : false;
        $sentencia = $is_update ? 'UPDATE' : 'INSERT INTO';
        $values['integrante']['inscripciones_id'] = $inscripciones_id;
        $values['integrante']['tipos_integrantes_id'] = $integrantes['tipo'][$i];
        $values['integrante']['nombre'] = $integrantes['nombre'][$i];
        $values['integrante']['apellido_paterno'] = $integrantes['apellido-paterno'][$i];
        $values['integrante']['apellido_materno'] = $integrantes['apellido-materno'][$i];
        $values['integrante']['fecha_nacimiento'] = format_date($integrantes['fecha-nacimiento'][$i]);

        // Guardar la fotografia.
        $filename = str_pad($i, 2, '0', STR_PAD_LEFT) . '-fotografia.';
        $dataUri = $integrantes['fotografia'][$i];

        if ($dataUri === 'default') {
          $url_fotografia = null;
          $path_fotografia = null;
          $saved_fotografia = true;
        } else if (filter_var($dataUri, FILTER_VALIDATE_URL)) {
          $url_fotografia = $dataUri;
          $path_fotografia = str_replace($host, '', $dataUri);
          $saved_fotografia = true;
        } else {
          $array_img = explode('base64,', $dataUri);
          $img = end($array_img);
          $encoded_data = str_replace(' ', '+', $img);
          $decoded_data = base64_decode($encoded_data);
          $size = strlen($decoded_data);

          foreach ($types['integrante'] as $extension => $formato) {
            if (strpos($array_img[0], $formato) !== FALSE) {
              // $extension = $extension;
              // $formato = $formato;
              $filename .= $extension;
              break 1;
            }
          }

          $path_fotografia = $paths['integrante'] . $filename;
          $url_fotografia = $host . $path_fotografia;
          $target = $root . $path_fotografia;

          $saved_fotografia = file_put_contents($target, $decoded_data, FILE_USE_INCLUDE_PATH);
        }

        // Guardar la credencial.
        $filename = str_pad($i, 2, '0', STR_PAD_LEFT) . '-credencial.';
        $dataUri = $integrantes['credencial'][$i];

        if ($dataUri === 'default') {
          $url_credencial = null;
          $path_credencial = null;
          $saved_credencial = true;
        } else if (filter_var($dataUri, FILTER_VALIDATE_URL)) {
          $url_credencial = $dataUri;
          $path_credencial = str_replace($host, '', $dataUri);
          $saved_credencial = true;
        } else {
          $array_img = explode('base64,', $dataUri);
          $img = end($array_img);
          $encoded_data = str_replace(' ', '+', $img);
          $decoded_data = base64_decode($encoded_data);
          $size = strlen($decoded_data);

          foreach ($types['integrante'] as $extension => $formato) {
            if (strpos($array_img[0], $formato) !== FALSE) {
              // $extension = $extension;
              // $formato = $formato;
              $filename .= $extension;
              break 1;
            }
          }

          $path_credencial = $paths['integrante'] . $filename;
          $url_credencial = $host . $path_credencial;
          $target = $root . $path_credencial;

          $saved_credencial = file_put_contents($target, $decoded_data, FILE_USE_INCLUDE_PATH);
        }

        if (!$saved_fotografia && !$saved_credencial) {
          $pdf->rollback();
          echo json_encode($errors['integrante']['save']);
          exit;
        }

        // Ultimos values...
        $values['integrante']['url_fotografia']  = $url_fotografia;
        $values['integrante']['path_fotografia'] = $path_fotografia;
        $values['integrante']['url_credencial']  = $url_credencial;
        $values['integrante']['path_credencial'] = $path_credencial;

        if ($is_update) {
          $where['integrante'] = "WHERE integrantes_id = :integrantes_id";
          $values['integrante']['integrantes_id'] = $id;
        }

        // Sentencia en ejecucion.
        $sql = $pdo->prepare($sentencia . " integrantes
          SET inscripciones_id = :inscripciones_id,
            tipos_integrantes_id = :tipos_integrantes_id,
            nombre = :nombre,
            apellido_paterno = :apellido_paterno,
            apellido_materno = :apellido_materno,
            fecha_nacimiento = NULLIF(:fecha_nacimiento, 'null'),
            url_fotografia = NULLIF(:url_fotografia, 'null'),
            path_fotografia = NULLIF(:path_fotografia, 'null'),
            url_credencial = NULLIF(:url_credencial, 'null'),
            path_credencial = NULLIF(:path_credencial, 'null'),
            borrado = 0 " .
          $where['integrante']);

        $ok = $sql->execute($values['integrante']);
        if (!$ok) {
          $pdo->rollback();
          echo json_encode($errors['integrante']['sql']);
          exit;
        }

        $integrantes_id = $id ? $id : $pdo->lastInsertId();
        // Log Integrantes.
        $insert = $pdo->prepare("INSERT INTO log_integrantes
          SELECT NULL, :sentencia, integrantes_id, inscripciones_id,
            tipos_integrantes_id, nombre, apellido_paterno, apellido_materno,
            fecha_nacimiento, url_fotografia, path_fotografia,
            url_credencial, path_credencial,
            borrado, :usuarios_id, CURRENT_TIMESTAMP
          FROM integrantes
          WHERE integrantes_id = :integrantes_id");
        $ok = $insert->execute(array(
            'sentencia' => $sentencia,
            'usuarios_id' => $usuarios_id,
            'integrantes_id' => $integrantes_id
          )
        );

        if (!$ok) {
          $pdo->rollback();
          echo json_encode(array(
              'status' => 'ERROR INSERTAR LOG INTEGRANTES',
              'msg' => '¡Algo sucedió! Por favor, vuelve a intentarlo de nuevo.'
            )
          );
          exit;
        }

        $values['integrante'] = array();
        $where['integrante'] = '';
      }
      /* FIN INTEGRANTES */

      $pdo->commit();
      $success['data'] = $inscripciones_id;
      $success['reload'] = $sentencia === 'INSERT INTO';
      echo json_encode($success);
    break;

    case 'generar-nuevo-usuario':
      $comunidades_id = $_POST['comunidad'];

      $query = $pdo->prepare("SELECT u.username, u.password
        FROM comunidades c
        INNER JOIN usuarios_comunidades uc ON uc.comunidades_id = c.comunidades_id
        INNER JOIN usuarios u ON u.usuarios_id = uc.usuarios_id
        WHERE c.comunidades_id = :comunidades_id");
      $ok = $query->execute(array('comunidades_id' => $comunidades_id));
      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER USUARIO',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' a generar el usuario.'
          )
        );
        exit;
      }

      $result = $query->fetch(PDO::FETCH_ASSOC);
      if ($result) {
        echo json_encode(array(
            'status' => 'DUPLICATE',
            'msg' => 'La comunidad <strong>' . $comunidad . '</strong> ya tiene' .
              ' usuario, y es: <strong>' . $result['username'] . '</strong>' .
              ' y la contraseña es: ' . $result['password']
          )
        );
        exit;
      }

      $pdo->beginTransaction();

      $username = 'FuegoNuevo2019';
      $password = mt_rand(10000000, 99999999);

      $insert = $pdo->prepare("INSERT INTO usuarios
        SET username = :username,
          password = :password,
          usuarios_niveles_id = 3");
      $ok = $insert->execute(array(
          'username' => $username,
          'password' => $password
        )
      );

      if (!$ok) {
        $pdo->rollback();
        echo json_encode(array(
            'status' => 'ERROR USER',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' a generar el usuario.'
          )
        );
        exit;
      }

      $usuarios_id = $pdo->lastInsertId();

      $insert = $pdo->prepare("INSERT INTO usuarios_comunidades
        SET usuarios_id = :usuarios_id,
          comunidades_id = :comunidades_id");
      $ok = $insert->execute(array(
          'usuarios_id' => $usuarios_id,
          'comunidades_id' => $comunidades_id
        )
      );

      if (!$ok) {
        $pdo->rollback();
        echo json_encode(array(
            'status' => 'ERROR USER COMUNITY',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' a generar el usuario.'
          )
        );
        exit;
      }

      $pdo->commit();
      echo json_encode(array(
          'status' => 'OK',
          'msg' => 'El usuario para la comunidad <strong>{comunidad}</strong>' .
            ' fue generado correctamente.<br><br>' .
            ' Usuario: <strong>' . $username . '</strong><br>' .
            ' Contraseña: <strong>' . $password . '</strong>'
        )
      );
    break;

    case 'exportar-ficha-inscripcion':
      $comunidades_id = $_POST['comunidad'];
      function getAge($birthDate) {
        $birthDate = explode("/", $birthDate);
        //get age from date or birthdate
        $age = date("md", strtotime($birthDate[2].'-'.$birthDate[1].'-'.$birthDate[0])) > date("md")
          ? ((date("Y") - $birthDate[2]) - 1)
          : (date("Y") - $birthDate[2]);
        return $age;
      }

      include 'PHPExcel/IOFactory.php';

      $fileType = 'Excel2007';
      $fileName = '../ficha-inscripcion.xlsx';

      // Crear el Lector.
      $objReader = PHPExcel_IOFactory::createReader($fileType);
      $objPHPExcel = $objReader->load($fileName);

      $query = $pdo->prepare("SELECT i.inscripciones_id AS id,
          t.nombre AS nombreTemplo, tt.nombre AS tipoTemplo,
          c.nombre AS nombreComunidad, tc.nombre AS tipoComunidad,
          CONCAT(co.nombre, ' ', co.apellido_paterno,' ', co.apellido_materno)
          AS nombreCoordinador, co.celular AS celularCoordinador,
          CONCAT(de.nombre, ' ', de.apellido_paterno,' ', de.apellido_materno)
          AS nombreDelegado, de.celular AS celularDelegado,
          (d.disciplinas_id - 1) AS sheet
        FROM inscripciones i
        INNER JOIN informacion_base ib ON ib.informacion_base_id = i.informacion_base_id
        INNER JOIN disciplinas d ON d.disciplinas_id = i.disciplinas_id
        INNER JOIN status_inscripciones si ON si.status_id = i.status_id
        INNER JOIN comunidades c ON c.comunidades_id = ib.comunidades_id
        INNER JOIN coordinadores co ON co.coordinadores_id = ib.coordinadores_id
        INNER JOIN delegados de ON de.delegados_id = ib.delegados_id
        INNER JOIN tipos_comunidades tc ON tc.tipos_comunidades_id = c.tipos_comunidades_id
        INNER JOIN templos t ON t.templos_id = c.templos_id
        INNER JOIN tipos_templos tt ON tt.tipos_templos_id = t.tipos_templos_id
        WHERE ib.comunidades_id = :comunidades_id
          AND i.borrado = 0");
      $ok = $query->execute(array(
          'comunidades_id' => $comunidades_id
        )
      );

      if (!$ok) {
        echo json_encode(array(
            'status' => 'ERROR OBTENER INSCRIPCIONES',
            'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
              ' exportar la ficha de inscripción.'
          )
        );
        exit;
      }

      $inscripciones = $query->fetchAll(PDO::FETCH_ASSOC);

      foreach ($inscripciones as $key => $inscripcion) {
        $sheet = $objPHPExcel->setActiveSheetIndex($inscripcion['sheet']);
        $sheet->setCellValue('C3', $inscripcion['nombreTemplo']);
        $sheet->setCellValue('C4', $inscripcion['tipoTemplo']);
        $sheet->setCellValue('C7', $inscripcion['nombreComunidad']);
        $sheet->setCellValue('C8', $inscripcion['tipoComunidad']);
        $sheet->setCellValue('C12', $inscripcion['nombreCoordinador']);
        $sheet->setCellValue('C13', $inscripcion['celularCoordinador']);
        $sheet->setCellValue('C16', $inscripcion['nombreDelegado']);
        $sheet->setCellValue('C17', $inscripcion['celularDelegado']);

        // Obtener los integrantes.
        $query = $pdo->prepare("SELECT ti.nombre AS tipoIntegrante,
            IF(ti.tipos_integrantes_id = 2, 1, 0) esCapitan,
            TRIM(CONCAT(i.nombre, ' ', i.apellido_paterno,' ', i.apellido_materno))
            AS nombre, DATE_FORMAT(i.fecha_nacimiento, '%d/%m/%Y') AS fechaNacimiento,
            path_fotografia AS fotografia, path_credencial AS credencial
          FROM integrantes i
          INNER JOIN tipos_integrantes ti ON ti.tipos_integrantes_id = i.tipos_integrantes_id
          WHERE i.inscripciones_id = :inscripciones_id
            AND i.borrado = 0");
         $ok = $query->execute(array(
            'inscripciones_id' => $inscripcion['id']
          )
        );

        if (!$ok) {
          echo json_encode(array(
              'status' => 'ERROR OBTENER INTEGRANTE ' . $key,
              'msg' => '¡Lo sentimos!, ¡algo sucedió! Por favor, vuelve a intentar' .
                ' exportar la ficha de inscripción.'
            )
          );
          exit;
        }

        $integrantes = $query->fetchAll(PDO::FETCH_ASSOC);
        $integrantes_row = 23;
        $integrantes_row_add = 7;

        foreach ($integrantes as $key => $integrante) {
          if (empty($integrante['nombre'])) {
            continue;
          }

          if ($integrante['esCapitan']) {
            $sheet->setCellValue('B21', $integrante['nombre']);
          }

          if ($key%2 === 0) {
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setPath($_SERVER['DOCUMENT_ROOT'] . '/fuegonuevo/' .
              $integrante['fotografia']);
            $objDrawing->setCoordinates('A' . ($integrantes_row));
            $objDrawing->setWidth(118);
            $objDrawing->setWorksheet($sheet);

            $sheet->setCellValue('B' . ($integrantes_row + 1),
              $integrante['nombre']);
            $sheet->setCellValue('C' . ($integrantes_row + 3),
              getAge($integrante['fechaNacimiento']));
            $sheet->setCellValue('B' . ($integrantes_row + 5),
              $integrante['fechaNacimiento']);
          } else {
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setPath($_SERVER['DOCUMENT_ROOT'] . '/fuegonuevo/' .
              $integrante['fotografia']);
            $objDrawing->setCoordinates('E' . ($integrantes_row));
            $objDrawing->setWidth(118);
            $objDrawing->setWorksheet($sheet);

            $sheet->setCellValue('F' . ($integrantes_row + 1),
              $integrante['nombre']);
            $sheet->setCellValue('G' . ($integrantes_row + 3),
              getAge($integrante['fechaNacimiento']));
            $sheet->setCellValue('F' . ($integrantes_row + 5),
              $integrante['fechaNacimiento']);

            $integrantes_row += $integrantes_row_add;
          }
        }
      }

      // Change the file
      // $objPHPExcel->setActiveSheetIndex(0)
      //             ->setCellValue('A1', 'Hello')
      //             ->setCellValue('B1', 'World!');

      // Guarda el archivo.
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);

      try {
        $newFileName = '../ficha-inscripcion-fn2017.xlsx';
        $objWriter->save($newFileName);
        echo json_encode(array(
            'status' => 'OK',
            'data' => $newFileName,
            'msg' => 'La ficha de inscripción fue generada correctamente.'
          )
        );
      } catch (PHPExcel_Writer_Exception $e) {
        echo json_encode(array(
            'status' => 'PHPExcel Error Exception',
            'error' => $e,
            'msg' => '¡Algo sucedió! Por favor, intenta de nuevo generar la ' .
              'ficha de inscripción.'
          )
        );
      }
    break;
  }
?>