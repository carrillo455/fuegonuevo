<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

$option = $_GET['o'];

switch ($option) {
  case 'inscripciones':
    /*
      ALTER VIEW dt_inscripciones AS
        SELECT 1 AS consecutivo, i.inscripciones_id AS id,
          c.nombre AS comunidad, CONCAT(co.nombre, ' ', co.apellido_paterno,
          ' ', co.apellido_materno) AS coordinador, d.nombre AS disciplina, d.rama AS rama,
          (SELECT SUM(IF(fecha_nacimiento IS NULL, 0, 1)) FROM integrantes WHERE inscripciones_id = i.inscripciones_id) AS numeroIntegrantes,
          si.nombre AS status
        FROM inscripciones i
        INNER JOIN informacion_base ib ON ib.informacion_base_id = i.informacion_base_id
        INNER JOIN comunidades c ON c.comunidades_id = ib.comunidades_id
        INNER JOIN coordinadores co ON co.coordinadores_id = ib.coordinadores_id
        INNER JOIN disciplinas d ON d.disciplinas_id = i.disciplinas_id
        INNER JOIN status_inscripciones si ON si.status_id = i.status_id
        WHERE i.borrado = 0;
    */
    $table = 'dt_inscripciones';

    $primaryKey = 'id';
    $columns = array(
      array('db' => 'consecutivo', 'dt' => 0),
      array('db' => 'id', 'dt' => 1),
      array('db' => 'comunidad', 'dt' => 2),
      array('db' => 'coordinador', 'dt' => 3),
      array('db' => 'disciplina', 'dt' => 4),
      array('db' => 'rama', 'dt' => 5),
      array('db' => 'numeroIntegrantes', 'dt' => 6),
      array('db' => 'status', 'dt' => 7)
    );
  break;

  case 'usuarios':
    /*
      ALTER VIEW dt_usuarios AS
        SELECT 1 AS consecutivo, u.usuarios_id AS id,
          u.username, u.password, c.nombre AS comunidad, tc.nombre AS tipo,
          IF(u.borrado = 0,
            CONCAT('<a class="small alert button" data-block=', u.usuarios_id,
            '>Bloquear</a>'),
            CONCAT('<a class="small secondary button" data-unblock=', u.usuarios_id,
            '>Desbloquear</a>')
          ) AS acciones
        FROM usuarios u
        INNER JOIN usuarios_comunidades uc ON uc.usuarios_id = u.usuarios_id
        INNER JOIN comunidades c ON c.comunidades_id = uc.comunidades_id
        INNER JOIN tipos_comunidades tc ON tc.tipos_comunidades_id = c.tipos_comunidades_id
        WHERE u.usuarios_niveles_id = 3;
    */
    $table = 'dt_usuarios';

    $primaryKey = 'id';
    $columns = array(
      array('db' => 'consecutivo', 'dt' => 0),
      array('db' => 'id', 'dt' => 1),
      array('db' => 'username', 'dt' => 2),
      array('db' => 'password', 'dt' => 3),
      array('db' => 'comunidad', 'dt' => 4),
      array('db' => 'tipo', 'dt' => 5),
      array('db' => 'acciones', 'dt' => 6)
    );
  break;

  case 'comunidades':
    /*
      ALTER VIEW dt_comunidades AS
        SELECT 1 AS consecutivo, c.comunidades_id AS id,
          t.nombre AS templo, c.nombre AS nombre, tc.nombre AS tipo,
          IF(c.borrado = 0,
            CONCAT('<a class="small alert button" data-delete=', c.comunidades_id,
            '>Borrar</a>'),
            CONCAT('<a class="small secondary button" data-undelete=', c.comunidades_id,
            '>Incorporar</a>')
          ) AS acciones
        FROM comunidades c
        INNER JOIN tipos_comunidades tc ON tc.tipos_comunidades_id = c.tipos_comunidades_id
        INNER JOIN templos t ON t.templos_id = c.templos_id;
    */
    $table = 'dt_comunidades';

    $primaryKey = 'id';
    $columns = array(
      array('db' => 'consecutivo', 'dt' => 0),
      array('db' => 'id', 'dt' => 1),
      array('db' => 'templo', 'dt' => 2),
      array('db' => 'nombre', 'dt' => 3),
      array('db' => 'tipo', 'dt' => 4),
      array('db' => 'acciones', 'dt' => 5)
    );
  break;
}

// SQL server connection information
require( '../conexion.php' );
$sql_details = array(
  'user' => $mysql['user'],
  'pass' => $mysql['pass'],
  'db'   => $mysql['db'],
  'host' => $mysql['host']
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );

echo json_encode(
  SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);

